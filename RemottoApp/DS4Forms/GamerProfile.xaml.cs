﻿using RemottoApp.DS4Forms.ViewModels;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using WpfTutorialSamples.Dialogs;
using static RemottoApp.DS4Forms.ColorPickerWindow;
using RemottoApp.DS4Forms;
using System.Windows.Shapes;
using System.Collections;
using System.Timers;
using System.Windows.Threading;


namespace RemottoApp.DS4Forms
{
    public partial class GamerProfile : DockPanel
    {
        private MainWindow mainWindow;
        private ControllerListViewModel conLvViewModel;
        private YoutuberListViewModel youtubersViewModel;
        private DS4Device device;
        int cont = 0;
        int cont2 = 0;
        bool one = false, two = false, tree = false, four = false;
        bool lagDetected = false;
        bool wasConnected = false;
        bool animSearch = false;
        bool flashDots = false;
        bool lowbattery = false;
        string nofoundcontroller = Translations.Strings.ControllerNotFound;
        string searchcontroller = Translations.Strings.SearchController;
        string lagmessage = RemottoApp.Translations.Strings.HighLag;        
        ArrayList lines = new ArrayList();
        public Line batteryLevelDots;
        public event ColorChangedHandler ColorChanged;
        public delegate void ColorChangedHandler(ColorPickerWindow sender, Color color);
        int millisecondsHelpLag = 3000;

        // Wheel variables and const
        const float DEG_TO_RAD = (float)((Math.PI * 2) / 360f);
        const float RATIO_IMAGEN = 554f / 1084f;
        const float CENTER_X = 554f / 2f;
        const float CENTER_Y = 554f / 2f;
        const float RADIO_OUT = (345f - 1.5f) * RATIO_IMAGEN;
        const float RADIO_IN = RADIO_OUT - (25f * RATIO_IMAGEN);

        private const float INICIO_ARCO_INICIAL = (float)(210f * DEG_TO_RAD);
        private const float LARGO_ARCO_INICIAL = (float)(240f * DEG_TO_RAD);

        const float CIRCUNFERENCIA = (float)(RADIO_OUT * 2f * Math.PI);
        const float ANCHO_SEGMENTO = CIRCUNFERENCIA * (240f / 360f) / (float)NUM_POINTS;
        const float ANGULO_SEGMENTO = LARGO_ARCO_INICIAL / (float)NUM_POINTS;

        const float ANCHO_LINEA_GORDA_IMAGEN = 14f * RATIO_IMAGEN;
        const float ANGULO_LINEA_GORDA_IMAGEN = (360f * DEG_TO_RAD) * (ANCHO_LINEA_GORDA_IMAGEN / CIRCUNFERENCIA);

        // Calculo ajustado tomando en cuenta el ancho de la pincelada
        private const float INICIO_ARCO = INICIO_ARCO_INICIAL - ANGULO_SEGMENTO / 2f;
        private const float LARGO_ARCO = LARGO_ARCO_INICIAL - ANGULO_SEGMENTO;
        private const float DIRECCION_ARCO = -1f;
        // Finish Wheel declarations

        public ControllerListViewModel ConLvViewModel { get => conLvViewModel; }
        public YoutuberListViewModel YoutubersViewModel { get => youtubersViewModel; set => youtubersViewModel = value; }
        public GamerProfile() 
        {
            InitializeComponent();
            drawWheel();
            SetTimer();
            BatteryLevels();
            SetTimerInputLag();
            //SetHelpForInputLag();
            SetTimerNoControllerFound();
            SetCTALowBattery();
            //SetTimerNoControllerFound2();
            //SetTimerNoControllerFound3();            
            CtaActionButton(nofoundcontroller);
            responsiveInit();
            LinkButton.Content = RemottoApp.Translations.Strings.LinkController;
            


        }

        public void setMainWindow(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
            SettingsButtonText.Text = RemottoApp.Translations.Strings.Custom;
            lagHelpButton.Content = RemottoApp.Translations.Strings.FixLag;
            
        }
      
        public void setupDataContext() 
        {
            
            //idController.DataContext = conLvViewModel;
            batteryInfo.DataContext = conLvViewModel;
            //statusInfo.DataContext = conLvViewModel;
            //colSelection.DataContext = conLvViewModel;
            //colorPicker.DataContext = conLvViewModel;
            int cont = 0;


            //colorPicker.R = Global.LightbarSettingsInfo[0].ds4winSettings.m_CustomLed.red;
            //colorPicker.G = Global.LightbarSettingsInfo[0].ds4winSettings.m_CustomLed.green;
            //colorPicker.B = Global.LightbarSettingsInfo[0].ds4winSettings.m_CustomLed.blue;
            //nameText.DataContext = youtubersViewModel;
            WrapYoutuber.DataContext = youtubersViewModel;
            //WrapSelf.DataContext = youtubersViewModel;
            goBackBtn.DataContext = youtubersViewModel;
            //devInfo.DataContext = youtubersViewModel; 
            //devYoutuberInfo.DataContext = youtubersViewModel;
            batteryCont.DataContext = youtubersViewModel;
            youtuberConfigs.DataContext = youtubersViewModel;
            statusCont.DataContext = youtubersViewModel;          
            userStack2.DataContext = youtubersViewModel;
            imagePanel.DataContext = youtubersViewModel;
            //colPanel.DataContext = youtubersViewModel;
            youtuberGamesList.DataContext = youtubersViewModel;
            youtubersList.DataContext = youtubersViewModel;


            

        }

        public void BindIcons() 
        { 
            gamesIconList.ItemsSource = youtubersViewModel.PrintYoutuber[0].GamesIcon;
            youtuberProfilesList.ItemsSource = youtubersViewModel.PrintYoutuber[0].GamesIcon;
        }
        
        public void UpdateLvViewModel(ControllerListViewModel conLvViewModel) 
        {
            this.conLvViewModel = conLvViewModel;
            if (youtubersViewModel != null)
                ChangeControllerPanel();               
        }

        public void UpdateYoutubersViewModel(YoutuberListViewModel youtubersViewModel)
        {
            this.youtubersViewModel = youtubersViewModel;
            this.youtubersViewModel.populateYoutubersColWithoutSelected();
            if (conLvViewModel != null)
                ChangeControllerPanel();
        }

        private void resetWheelColor()
        {
            LinearGradientBrush noncolor = new LinearGradientBrush();
            noncolor.GradientStops.Add(new GradientStop(Color.FromArgb(0, 0, 0, 0), 0.0));
            for (int i = 0; i < NUM_POINTS; i++)
            {

                ((Line)lines[i]).Stroke = Brushes.Transparent;
            }

        }

        private void BatteryLevels()
        {
            
            if (ConLvViewModel == null || ConLvViewModel.ControllerCol.Count != 1)
            {
               
                //CtaActionButton(nofoundcontroller);
                lagHelpButton.Visibility = Visibility.Hidden;
                SettingsButton.Visibility = Visibility.Hidden;
                imageSettings.Visibility = Visibility.Hidden;
                LinkButton.Visibility = Visibility.Visible;
                if(wasConnected == true)
                {
                    //resetWheelColor();                    
                    wasConnected = false;
                }
                
            }
            else
            {
                if(ConLvViewModel.ControllerCol.Count > 0)
                {
                    Light.Visibility = Visibility.Visible;
                    wasConnected = true;
                    //CtaActionButton("");
                    //CTADialog.Visibility = Visibility.Hidden;
                    SettingsButton.Visibility = Visibility.Visible;
                    imageSettings.Visibility = Visibility.Visible;
                    LinkButton.Visibility = Visibility.Collapsed;
                    if(lagDetected == true)
                    {
                        //CtaActionButton("Input lag demasiado alto!");
                        //CTADialog.Visibility = Visibility.Visible;
                        //lagHelpButton.Visibility = Visibility.Visible;
                    }
                  
                    if (ConLvViewModel.ControllerCol[0].BatteryState == "")
                    {

                    }
                    else
                    {

                        string batterylevel = ConLvViewModel.ControllerCol[0].BatteryState;
                        if (batterylevel.Contains("%+"))
                        {
                            flashBatteryDots(batterylevel);
                        }
                        else
                        {
                            if(batterylevel.Length == 4 && !batterylevel.Contains("%+"))
                            {
                                batterylevel = batterylevel.Remove(3);
                            }
                            else
                            {
                                if(batterylevel == "0%")
                                {
                                    batterylevel = batterylevel.Remove(1);
                                }
                                else
                                {
                                    batterylevel = batterylevel.Remove(2);
                                }
                                
                            }

                            if(Int32.Parse(batterylevel) <= 25)
                            {                               
                                if(flashDots == false)
                                {
                                    lowbattery = true;
                                    BatteryBlock0.Visibility = Visibility.Visible;
                                    BatteryBlock1.Visibility = Visibility.Visible;
                                    BatteryBlock2.Visibility = Visibility.Visible;
                                    BatteryBlock3.Visibility = Visibility.Visible;
                                    BatteryBlock4.Visibility = Visibility.Visible;
                                    flashDots = true;
                                }
                                else
                                {
                                    BatteryBlock0.Visibility = Visibility.Visible;
                                    BatteryBlock1.Visibility = Visibility.Hidden;
                                    BatteryBlock2.Visibility = Visibility.Hidden;
                                    BatteryBlock3.Visibility = Visibility.Hidden;
                                    BatteryBlock4.Visibility = Visibility.Hidden;
                                    flashDots = false;

                                }
                                

                            }                           
                            if ((Int32.Parse(batterylevel) > 25 && Int32.Parse(batterylevel) <= 50) && flashDots == false)
                            {
                                BatteryBlock1.Visibility = Visibility.Visible;
                                BatteryBlock2.Visibility = Visibility.Hidden;
                                BatteryBlock3.Visibility = Visibility.Hidden;
                                BatteryBlock4.Visibility = Visibility.Hidden;
                            }
                            if (Int32.Parse(batterylevel) >= 50)
                            {
                                BatteryBlock1.Visibility = Visibility.Visible;
                                BatteryBlock2.Visibility = Visibility.Visible;
                                BatteryBlock3.Visibility = Visibility.Hidden;
                                BatteryBlock4.Visibility = Visibility.Hidden;
                            }
                            if (Int32.Parse(batterylevel) >= 75)
                            {
                                BatteryBlock1.Visibility = Visibility.Visible;
                                BatteryBlock2.Visibility = Visibility.Visible;
                                BatteryBlock3.Visibility = Visibility.Visible;
                                BatteryBlock4.Visibility = Visibility.Hidden;
                            }
                            if (Int32.Parse(batterylevel) == 100)
                            {
                                BatteryBlock1.Visibility = Visibility.Visible;
                                BatteryBlock2.Visibility = Visibility.Visible;
                                BatteryBlock3.Visibility = Visibility.Visible;
                                BatteryBlock4.Visibility = Visibility.Visible;
                            }
                        }
                        

                    }
                }
                
            }
           

           






        }
       
        private void flashBatteryDots(string level)
        {

           if(one == false)
            {
                BatteryBlock1.Visibility = Visibility.Visible;
                BatteryBlock2.Visibility = Visibility.Hidden;
                BatteryBlock3.Visibility = Visibility.Hidden;
                BatteryBlock4.Visibility = Visibility.Hidden;
                one = true;
            }
            else if(one == true && two == false)
            {
                BatteryBlock1.Visibility = Visibility.Visible;
                BatteryBlock2.Visibility = Visibility.Visible;
                BatteryBlock3.Visibility = Visibility.Hidden;
                BatteryBlock4.Visibility = Visibility.Hidden;
                two = true;
            }
            else if (one == true && two == true && tree == false)
            {
                BatteryBlock1.Visibility = Visibility.Visible;
                BatteryBlock2.Visibility = Visibility.Visible;
                BatteryBlock3.Visibility = Visibility.Visible;
                BatteryBlock4.Visibility = Visibility.Hidden;
                tree = true;
            }
            else if (one == true && two == true && tree == true && four == false)
            {
                BatteryBlock1.Visibility = Visibility.Visible;
                BatteryBlock2.Visibility = Visibility.Visible;
                BatteryBlock3.Visibility = Visibility.Visible;
                BatteryBlock4.Visibility = Visibility.Visible;
                one = false;
                two = false;
                tree = false;
            }

        }

        const double MAXLAT = 31;
        const float POINTS_DIVIDER = 4f;
        const float NUM_POINTS = (float)(MAXLAT * POINTS_DIVIDER);
        double prevLatency = 0.0;
        double prevLatencyNoScale = 0.0;
        private void readInputLag()
        {
            //RemottoApp.Program.rootHub.latency
            double lat = RemottoApp.Program.rootHub.latency;
            prevLatencyNoScale = (prevLatencyNoScale + (lat - prevLatencyNoScale) * 0.05);


            lagValue.Content = prevLatencyNoScale.ToString("00");

            if (prevLatencyNoScale > 30.00)
            {
                lagValue.Content = "30+";
            }

            //lagValue.Content = prevLatencyNoScale;
            //string lagmessage = "                       Input lag demasiado alto!3";

            //lat = random.Next() * MAXLAT;
            lat = Math.Min(lat, MAXLAT * POINTS_DIVIDER);
            //lat = lat / MAXLAT;
            //lat = Math.Floor(lat);
            lat = prevLatency + (lat * POINTS_DIVIDER - prevLatency) * 0.05;
            //lat = 30.00;
            prevLatency = lat;
            if (lat < 100 * POINTS_DIVIDER)
            {
                int d = 0;
                LinearGradientBrush test = new LinearGradientBrush();
                LinearGradientBrush noncolor = new LinearGradientBrush();
                noncolor.GradientStops.Add(new GradientStop(Color.FromArgb(0,0,0,0), 0.0));
                if (ConLvViewModel != null)
                {
                    if (ConLvViewModel.ControllerCol.Count < 1)
                    {
                        test.StartPoint = new Point(0, 0);
                        test.EndPoint = new Point(1, 1);
                        test.GradientStops.Add(new GradientStop(Color.FromArgb(0, 0, 0, 0), 0.0));
                       
                    }
                    else
                    {
                        if (lat < 15.00 * POINTS_DIVIDER)
                        {

                            lagDetected = false;
                            test.StartPoint = new Point(0, 0);
                            test.EndPoint = new Point(1, 1);
                            test.GradientStops.Add(new GradientStop(Color.FromRgb(109, 223, 0), 0.0));
                            //lagHelpButton.Visibility = Visibility.Hidden;
                            //test.GradientStops.Add(new GradientStop(Colors.Yellow, 1.0));
                            //test.GradientStops.Add(new GradientStop(Colors.Red, 1.0));
                        }
                        else
                        {
                            if (lat > 15.00 * POINTS_DIVIDER || lat < 30.00 * POINTS_DIVIDER)
                            {
                                //lagDetected = false;
                                test.StartPoint = new Point(0, 0);
                                test.EndPoint = new Point(1, 1);
                                test.GradientStops.Add(new GradientStop(Color.FromRgb(247, 236, 10), 0.0));

                                //test.GradientStops.Add(new GradientStop(Colors.Yellow, 1.0));
                                //test.GradientStops.Add(new GradientStop(Colors.Red, 1.0));
                            }
                            if (lat > 15.00 * POINTS_DIVIDER)
                            {
                                lagDetected = true;
                              
                            }
                            if (lat > 30.00 * POINTS_DIVIDER)
                            {


                                test.StartPoint = new Point(0, 0);
                                test.EndPoint = new Point(1, 1);
                                test.GradientStops.Add(new GradientStop(Color.FromRgb(239, 26, 7), 0.0));
                                //test.GradientStops.Add(new GradientStop(Colors.Yellow, 1.0));
                                //test.GradientStops.Add(new GradientStop(Colors.Red, 1.0));
                            }

                        }
                    }

                    for (int i = 0; i < NUM_POINTS; i++)
                    {

                        ((Line)lines[i]).Stroke = i < lat ? ((Line)lines[i]).Stroke = test : ((Line)lines[i]).Stroke = Brushes.Transparent;
                    }

                }
            }
        

           


        }

        private void lagHelpWindow(object sender, RoutedEventArgs e)
        {
            lagTips help = new lagTips();
            help.Show();
            help.setMainWindow(this.mainWindow);
        }

        private void SetTimer()
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(10);
			timer.Tick += OnTimedEvent;
			timer.Start();
        }

        private void SetTimerInputLag()
        {
            DispatcherTimer timer = new DispatcherTimer();  
            timer.Interval = TimeSpan.FromMilliseconds(1000);
            timer.Tick += OnTimedEventInputLag;
            timer.Start();
        } 

        private void SetCTALowBattery()
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(7000);
            timer.Tick += OnTimedEventLowBatteryNLagTips;
            timer.Start();

        }

        private void SetHelpForInputLag()
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(8000);
            timer.Tick += OnTimedEventSetHelpForInputLag;
            timer.Start();
        } 

     

        private void SetTimerNoControllerFound()
        {
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(8000);
            timer.Tick += OnTimedEventNoControllerFound;
            timer.Start();
        }
     

        private void OnTimedEvent(object sender, EventArgs e)
        {
            readInputLag();

        }

        private void OnTimedEventInputLag(object sender, EventArgs e)
        {
            readInputLag();
            BatteryLevels();
           /* double templag = RemottoApp.Program.rootHub.latency;

            if (templag > 30.00)
            {
                lagValue.Content = "30+";
            }
            else
            {

                if ((Int32)RemottoApp.Program.rootHub.latency < 10)
                {
                    string lag = RemottoApp.Program.rootHub.latency.ToString();
                    lagValue.Content = "0" + lag[0];
                }
                else
                {
                    string lag = RemottoApp.Program.rootHub.latency.ToString();
                    lagValue.Content = lag[0] + "" + lag[1];
                }

            }*/
          

           
        }

        private void OnTimedEventLowBatteryNLagTips(object sender, EventArgs e)
        {
            if(ConLvViewModel.ControllerCol != null)
            {
           
                if(ConLvViewModel.ControllerCol.Count >= 1)
                {


                    switch (lagDetected)
                    {
                        case true:
                            Console.Write("test");
                            if (lagDetected == true)
                            {
                                //CtaActionButton(lagmessage);
                                CTA.Height = 70;
                                CTA.Text = lagmessage;
                                CTADialog.Visibility = Visibility.Visible;
                                lagHelpButton.Visibility = Visibility.Visible;
                                RemottoBatteryLink.Visibility = Visibility.Collapsed;
                            }
                            break;

                        case false:
                            if(lowbattery == true)
                            {
                                //Console.Write("Test2");
                                string batterylevel = Convert.ToString(ConLvViewModel.ControllerCol[0].BatteryLevel);
                                if (batterylevel.Contains("%"))
                                {
                                    lowbattery = false;
                                }
                                else
                                {
                                    if (batterylevel.Length == 3)
                                    {
                                        batterylevel = batterylevel.Remove(2);
                                    }

                                    if (Convert.ToInt32(batterylevel) <= 25)
                                    {
                                        lowbattery = true;
                                        CTA.Height = 110;
                                        CTA.Text = RemottoApp.Translations.Strings.LowBatteryCTA;
                                        RemottoBatteryLink.Content = Translations.Strings.BuyRemottoBattery;
                                        RemottoBatteryLink.Visibility = Visibility.Visible;
                                        CTADialog.Visibility = Visibility.Visible;
                                        lagHelpButton.Visibility = Visibility.Hidden;
                                    }
                                    else
                                    {
                                        CTA.Text = "";
                                        CTADialog.Visibility = Visibility.Hidden;
                                        RemottoBatteryLink.Visibility = Visibility.Hidden;
                                        // RemottoBatteryLink.Visibility.Collapsed;
                                    }
                                }
                            }
                            else
                            {
                                CTA.Height = 70;
                                CTA.Text = "";
                                CTADialog.Visibility = Visibility.Hidden;
                                lagHelpButton.Visibility = Visibility.Hidden;
                                RemottoBatteryLink.Visibility = Visibility.Collapsed;
                            }
                           
                            break;

                    }

                    


                    
                }               
            }
            
        }


        private void OnTimedEventSetHelpForInputLag(object sender, EventArgs e)
        {
           
            if (lagDetected == true)
            {
                CTA.Height = 70;
                //millisecondsHelpLag = millisecondsHelpLag * 2;
                CtaActionButton(lagmessage);
                //lagDetected = false;
                CTA.Text = lagmessage;
                CTADialog.Visibility = Visibility.Visible;
                //lagDetected = true;
                lagHelpButton.Visibility = Visibility.Visible;                
                RemottoBatteryLink.Visibility = Visibility.Collapsed;
                lagDetected = false;
            }
            else
            {
                if(ConLvViewModel != null)
                {
                    if (ConLvViewModel.ControllerCol.Count < 1)
                    {

                    }
                    else
                    {
                        CtaActionButton("");
                        lagHelpButton.Visibility = Visibility.Hidden;
                    }
                        

                }
                
            }
        }

        private void OnTimedEventNoControllerFound(Object sender, EventArgs e)
        {
            if (ConLvViewModel != null)
            {
                if(ConLvViewModel.ControllerCol.Count < 1 && animSearch == false)
                {                   
                    VelocimeterControllerEmpty.Visibility = Visibility.Collapsed;
                    //VelocimeterEmpty.Visibility = Visibility.Hidden;

                    CTA.Height = 70;
                    CtaActionButton(searchcontroller);

                    FirstWave.Visibility = Visibility.Visible;


                    animSearch = true;
                }
                else
                {
                    if(animSearch == true)
                    {
                        FirstWave.Visibility = Visibility.Collapsed;
                        VelocimeterControllerEmpty.Visibility = Visibility.Visible;
                        CTA.Height = 70;
                        CtaActionButton(nofoundcontroller);
                        animSearch = false;
                    }
                    
                }
            }
            else
            {
                CTA.Height = 70;
                CtaActionButton(nofoundcontroller);
                FirstWave.Visibility = Visibility.Collapsed;
                VelocimeterControllerEmpty.Visibility = Visibility.Visible;
                RemottoBatteryLink.Visibility = Visibility.Hidden;
                //VelocimeterEmpty.Visibility = Visibility.Collapsed;
            }



        }


        private void responsiveInit()
        {
           double widthOriginal = System.Windows.SystemParameters.PrimaryScreenWidth;
           double heigthOriginal = System.Windows.SystemParameters.PrimaryScreenHeight;

            if(widthOriginal >= 1920 && heigthOriginal >= 1080)
            {
               /* VelocimeterControllerEmpty.Width = widthOriginal - 650;
                VelocimeterControllerEmpty.Height = heigthOriginal - 650;

                Velocimeter.Width = widthOriginal - 650;
                Velocimeter.Height = heigthOriginal - 650;
                controllerImage.Width = 160;
                controllerImage.Height = 125;*/
                gridController.Margin = new Thickness(0, 110, 0, 0);

            }
            else
            {
                gridController.Margin = new Thickness(0, 10, 0, 0);
            }


  




        }


        private void drawWheel()
        {
            for (int i = 0; i < NUM_POINTS; i++)
            {
                float porcentajeProgreso = i / (NUM_POINTS - 1);
                float angle = INICIO_ARCO + DIRECCION_ARCO * LARGO_ARCO * porcentajeProgreso;

                Line myLine = new Line();
                myLine.Stroke = System.Windows.Media.Brushes.Transparent;
                myLine.X1 = CENTER_X + Math.Cos(angle) * RADIO_IN;
                myLine.X2 = CENTER_X + Math.Cos(angle) * RADIO_OUT;
                myLine.Y1 = CENTER_Y - Math.Sin(angle) * RADIO_IN;
                myLine.Y2 = CENTER_Y - Math.Sin(angle) * RADIO_OUT;

                myLine.StrokeThickness = ANCHO_SEGMENTO + (i == 0 || i == (NUM_POINTS - 1) ? ANCHO_LINEA_GORDA_IMAGEN : 0) + 0.5f;
                myLine.VerticalAlignment = VerticalAlignment.Top;

                lines.Add(myLine);
                GradientHost.Children.Add(myLine);
            }

            ((Line)lines[0]).Stroke = System.Windows.Media.Brushes.Transparent;


            //PathFigure test = new PathFigure();


        }


        private void CtaActionButton(string message)
        {
            if(ConLvViewModel == null || ConLvViewModel.ControllerCol.Count < 1 || message.Contains("Input lag"))
            {
                CTA.Height = 70;
                CTA.Text = message;
                CTADialog.Visibility = Visibility.Visible;

            }
            else
            {
                if(lagDetected == true)
                {
                    //CTA.Text = lagmessage;
                    //CTADialog.Visibility = Visibility.Visible;
                }
                else
                {
                 
                    if (ConLvViewModel.ControllerCol.Count > 0 && !message.Contains("Input lag") && lowbattery == false)
                    {
                        CTA.Height = 70;
                        CTA.Text = "";
                        CTADialog.Visibility = Visibility.Hidden;
                        lagHelpButton.Visibility = Visibility.Hidden;
                    }
                   
  
                }
                
            }
            

        }



        private void IdColumnTxtB_ToolTipOpening(object sender, ToolTipEventArgs e)
        {
            TextBlock statusBk = sender as TextBlock;
            int idx = Convert.ToInt32(statusBk.Tag);
            if (idx >= 0)
            {
                CompositeDeviceModel item = conLvViewModel.ControllerDict[idx];
                item.RequestUpdatedTooltipID();
            }
        }

        private void openYoutuber_Click(object sender, RoutedEventArgs e)
        {
            Button youtuber = sender as Button;
            int index = (int)youtuber.Tag;
            youtubersViewModel.selectYoutuber(index);
            this.UpdateYoutubersViewModel(youtubersViewModel);
            this.BindIcons();

            mainInfoPanel.Visibility = Visibility.Visible;
            profilePanel.Visibility = Visibility.Collapsed;
            mainWindow.mainTabCon.SelectedIndex = 1;
        }

        private void LightColorBtn_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            int idx = Convert.ToInt32(button.Tag);
            CompositeDeviceModel item = conLvViewModel.ControllerDict[idx];
            item.CustomColorItemClick(this, e);
        }


        private void ContStatusImg_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            CompositeDeviceModel item = conLvViewModel.CurrentItem;
            item.RequestDisconnect();
        }     

        public void helpmeBtn_Click(object sender, RoutedEventArgs e)
        {
            PopUp wdiag = new PopUp();
            wdiag.setMainWindow(this.mainWindow);
            wdiag.connectState();
            wdiag.ShowDialog();
        }


        public void userDataPopup(object sender, RoutedEventArgs e)
        {
            PopUp wdiag = new PopUp();
            wdiag.setMainWindow(this.mainWindow);
            wdiag.ShowDialog();
        }

        private void PurgeBatteryImages()
        {
            
            Light.Visibility = Visibility.Collapsed;
            BatteryBlock0.Visibility = Visibility.Collapsed;
            BatteryBlock1.Visibility = Visibility.Collapsed;
            BatteryBlock2.Visibility = Visibility.Collapsed;
            BatteryBlock3.Visibility = Visibility.Collapsed;
            BatteryBlock4.Visibility = Visibility.Collapsed;
        }
        private void ChangeControllerPanel()
        {
            EditControllerImage();
            FirstWave.Visibility = Visibility.Collapsed;
           
            if (conLvViewModel.ControllerCol.Count == 0 && youtubersViewModel.PrintYoutuber[0] == youtubersViewModel.GamersCol[0])
            {
                PurgeBatteryImages();
                infoCont.Visibility = Visibility.Hidden;
   
                if (RemottoApp.Properties.Settings.Default.alreadyConnected)
                {
                    //VelocimeterEmpty.Visibility = Visibility.Visible;
                    VelocimeterControllerEmpty.Visibility = Visibility.Visible;
                    CtaActionButton(nofoundcontroller);
                    Velocimeter.Visibility = Visibility.Collapsed;
                    lowbattery = false;
                    RemottoBatteryLink.Visibility = Visibility.Hidden;
                    //colorPicker.Visibility = Visibility.Hidden;
                    //noContLb.Visibility = Visibility.Visible;
                    
                }
                else
                {
                    noPairLb.Visibility = Visibility.Visible;
                    VelocimeterControllerEmpty.Visibility = Visibility.Visible;
                    Velocimeter.Visibility = Visibility.Collapsed;
                    lowbattery = false;
                    CTA.Visibility = Visibility.Visible;
                    CtaActionButton(nofoundcontroller);
                    LinkButton.Visibility = Visibility.Visible;

                    //colorPicker.Visibility = Visibility.Hidden;
                    //borderPicker.Visibility = Visibility.Hidden;
                }
               
            }
            else
            {
                FirstWave.Visibility = Visibility.Collapsed;
                //VelocimeterEmpty.Visibility = Visibility.Collapsed;
                VelocimeterControllerEmpty.Visibility = Visibility.Collapsed;
                CtaActionButton("");
                if(lowbattery == true)
                {
                    //CTADialog.Visibility = Visibility.Visible;
                    //lowbattery = false;
                }
                else
                {
                    CTADialog.Visibility = Visibility.Hidden;
                }
            
                       
                lagHelpButton.Visibility = Visibility.Hidden;
                animSearch = false;
                Velocimeter.Visibility = Visibility.Visible;
                infoCont.Visibility = Visibility.Visible;                
                //noContLb.Visibility = Visibility.Hidden;
                noPairLb.Visibility = Visibility.Collapsed;
            }
        }

        private void ShowProfile_Click(object sender, RoutedEventArgs e)
        {
            Button openProfile = sender as Button;
            int index = youtubersViewModel.PrintYoutuber[0].GamesIcon.IndexOf(openProfile.Tag.ToString());
            mainInfoPanel.Visibility = Visibility.Collapsed;
            Profile.Source = new BitmapImage(new Uri(youtubersViewModel.PrintYoutuber[0].Profiles[index], UriKind.Relative));
            Profile.DataContext = youtubersViewModel;
            profilePanel.Visibility = Visibility.Visible;
        }

        private void GoBack_Click(object sender, RoutedEventArgs e)
        {
            if (profilePanel.Visibility == Visibility.Visible)
            {
                profilePanel.Visibility = Visibility.Collapsed;
                mainInfoPanel.Visibility = Visibility.Visible;
            }
            else if (mainWindow.screenTag == 1)
            {
                mainWindow.openYoutubers_Click(sender, e);
            }
            else
            {
                mainWindow.myController_Click(sender, e);
            }
        }

        private void buyRemottoClick(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://es.remottobattery.com/products/remotto-battery");
        }
        public void editSelfName(string name)
        {
         
            this.youtubersViewModel.PrintYoutuber[0].setName(name);
            NameLabel.Text = name;
            //nameText.Text = name;
        }

        private void EditControllerImage()
        {
            //"Ampeterby7TheGrefgAgustin51StaXxAuronplay".Contains(youtubersViewModel.PrintYoutuber[0].Name)
            if ("Ampeterby7TheGrefgAgustin51StaXxAuronplay".Contains(youtubersViewModel.PrintYoutuber[0].Name) && !youtubersViewModel.PrintYoutuber[0].ControllerIcon.Contains("Default"))
            {
                //colorPicker.Visibility = Visibility.Hidden;
                string nameyoupad = "/RemottoApp;component/Resources/gamepads/" + youtubersViewModel.PrintYoutuber[0].Name + ".png";
                this.controllerImage.Source = new BitmapImage(new Uri(nameyoupad, UriKind.Relative));
            }
            else if (conLvViewModel.ControllerCol.Count != 0)
            {                                
                if (ConLvViewModel.ControllerCol[0].IdText.Contains("DualSense"))
                {
                    string name = "/RemottoApp;component/Resources/gamepads/DefaultDualSense.png";                 
                    this.controllerImage.Source = new BitmapImage(new Uri(name, UriKind.Relative));
                    //colorPicker.Visibility = Visibility.Visible;
                }
                if(ConLvViewModel.ControllerCol[0].IdText.Contains("DS4"))
                {
                    string name2 = "/RemottoApp;component/Resources/gamepads/Default.png";                    
                    this.controllerImage.Source = new BitmapImage(new Uri(name2, UriKind.Relative));
                    //colorPicker.Visibility = Visibility.Visible;
                }          
            }            
        }
        private void editName_Click(object sender, RoutedEventArgs e)
        {
            //Global.= NameLabelEdit.Text;
         
            if (NameLabel.Visibility.Equals(Visibility.Hidden))
            {
                NameLabel.Visibility = Visibility.Visible;
                NameLabelEdit.Visibility = Visibility.Collapsed;
                
            }
            else
            {
                if(cont2 == 1)
                {
                    NameLabelEdit.Text = Properties.Settings.Default.UserName;
                    //Properties.Settings.Default.UserName = NameLabelEdit.Text;
                    //Properties.Settings.Default.Save();
                    //editSelfName(NameLabelEdit.Text);
                }
                cont2++;
                NameLabelEdit.Text = Properties.Settings.Default.UserName;
                NameLabel.Visibility = Visibility.Hidden;
                NameLabelEdit.Visibility = Visibility.Visible;
               
            }
           

           

            /*InputDialogSample inputDialog = new InputDialogSample("{ lex:Loc EnterYourName}", "");
            if (inputDialog.ShowDialog() == true) {
                Properties.Settings.Default.UserName = inputDialog.Answer;
                Properties.Settings.Default.Save();
                editSelfName(inputDialog.Answer);
            }*/

            //NameLabel.E
        }

        private void openColorPicker(object sender, RoutedEventArgs e)
        {
            ColorPicker test = new ColorPicker();
            test.Show();
            test.setMainWindow(this.mainWindow);
        }

        private void closeColorPicker(object sender, RoutedEventArgs e)
        {
           
                Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    this.mainWindow.Opacity = 1;
                }));
            
        }

        private void ListViewScrollViewer_PreviewMouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            ScrollViewer scv = (ScrollViewer)sender;
            scv.ScrollToHorizontalOffset(scv.HorizontalOffset - e.Delta);
            e.Handled = true;
        }

        private void NameLabelEdit_LostFocus(object sender, RoutedEventArgs e)
        {
            NameLabelEdit.Visibility = Visibility.Collapsed;
            NameLabel.Visibility = Visibility.Visible;
            Properties.Settings.Default.UserName = NameLabelEdit.Text;
            Properties.Settings.Default.Save();
            editSelfName(NameLabelEdit.Text);

        }

        private void selectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
           if(cont == 0)
            {
                cont += 1;
            }
            else
            {
                Global.LightbarSettingsInfo[0].ds4winSettings.m_CustomLed = new DS4Color() { red = e.NewValue.Value.R, green = e.NewValue.Value.G, blue = e.NewValue.Value.B };
                Global.Save();
                //Global.LightbarSettingsInfo[0].ds4winSettings.m_CustomLed = new DS4Color() { red = e.NewValue.Value.R, green = e.NewValue.Value.G, blue = e.NewValue.Value.B };
            }
            
            
        }
    }    
}
