﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Navigation;
using System.Windows.Input;
using HttpProgress;
using NonFormTimer = System.Timers.Timer;
using System.Windows.Controls;

namespace RemottoApp.DS4Forms
{
    /// <summary>
    /// Interaction logic for WelcomeDialog.xaml
    /// </summary>
    public partial class UpdateVersion : Window
    {
        private MainWindow mainWindow;
        private bool IsNonCloseButtonClicked;
        public UpdateVersion()
        {    
            InitializeComponent();
        }

        public void setMainWindow(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
        }

        private void Update_Click(object sender, RoutedEventArgs e)
        {
            IsNonCloseButtonClicked = true;
            this.Close();
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        
        private void Close(object sender, EventArgs e)
        {
            if (!IsNonCloseButtonClicked)
            {
                Environment.Exit(0);
            }
        }
    }
}
