﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using RemottoApp.DS4Forms;

namespace RemottoApp.DS4Forms
{
    /// <summary>
    /// Interaction logic for ColorPicker.xaml
    /// </summary>
    public partial class ColorPicker : Window
    {
        private int cont;
        private MainWindow mainWindow;

        public ColorPicker()
        {
            InitializeComponent();
            colorPickerSelection.R = Global.LightbarSettingsInfo[0].ds4winSettings.m_CustomLed.red;
            colorPickerSelection.G = Global.LightbarSettingsInfo[0].ds4winSettings.m_CustomLed.green;
            colorPickerSelection.B = Global.LightbarSettingsInfo[0].ds4winSettings.m_CustomLed.blue;
        }

        public void setMainWindow(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
            this.mainWindow.Opacity = 0.4;
        }

        private void selectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            
            if (cont == 0)
            {
                cont += 1;
            }
            else
            {
                Global.LightbarSettingsInfo[0].ds4winSettings.m_CustomLed = new DS4Color() { red = e.NewValue.Value.R, green = e.NewValue.Value.G, blue = e.NewValue.Value.B };
                Global.Save();
                //Global.LightbarSettingsInfo[0].ds4winSettings.m_CustomLed = new DS4Color() { red = e.NewValue.Value.R, green = e.NewValue.Value.G, blue = e.NewValue.Value.B };
            }
             

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
            this.mainWindow.Opacity = 1;
        }

        protected override void OnDeactivated(EventArgs e)
        {
            base.OnDeactivated(e);
            this.mainWindow.Opacity = 1;
            this.Hide();
        }
    }
}
