﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RemottoApp.DS4Forms
{
    /// <summary>
    /// Interaction logic for ExitWindow.xaml
    /// </summary>
    public partial class ExitWindow : Window
    {
        public event EventHandler buttonClickYes;
        public event EventHandler buttonClicNo;
        private MainWindow mainWindow;
        private static object parser;

        public ExitWindow()
        {
            InitializeComponent();
            yesButton.Text = RemottoApp.Translations.Strings.Close;
            buttonYes.Content = RemottoApp.Translations.Strings.YesCloseApp;
            buttonno.Content = RemottoApp.Translations.Strings.NoCloseApp;
        }

        public void setMainWindow(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
            mainWindow.Opacity = 0.4;

        }

        public void YesButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            buttonClickYes?.Invoke(this, EventArgs.Empty);
            
            Console.WriteLine("Yes");
            Application.Current.Shutdown();
            
        }

        private void NoButton_Click(object sender, RoutedEventArgs e)
        {
            buttonClicNo?.Invoke(this, EventArgs.Empty);

            Console.WriteLine("No");
            mainWindow.Opacity = 1;
            Close();
            
        }

       
        protected override void OnDeactivated(EventArgs e)
        {
            mainWindow.Opacity = 1;
            this.Hide();   
            base.OnDeactivated(e);

        }
    }
}
