﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Windows;
using System.Windows.Navigation;
using System.Windows.Input;
using HttpProgress;
using NonFormTimer = System.Timers.Timer;
using Garlic;
using System.Net.Mail;

namespace RemottoApp.DS4Forms
{
    /// <summary>
    /// Interaction logic for WelcomeDialog.xaml
    /// </summary>
    public partial class PopUp : Window
    {
        private const string InstallerDL =
            "https://github.com/ViGEm/ViGEmBus/releases/download/setup-v1.16.116/ViGEmBus_Setup_1.16.116.exe";
        private const string InstFileName = "ViGEmBus_Setup_1.16.116.exe";
        private string tempInstFileName;

        private MainWindow mainWindow;

        Process monitorProc;
        NonFormTimer monitorTimer;
       
        public PopUp(bool loadConfig = false)
        {
            if (loadConfig)
            {
                RemottoApp.Global.FindConfigLocation();
                RemottoApp.Global.Load();
                //DS4Windows.Global.SetCulture(DS4Windows.Global.UseLang);
            }

            InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;

            tempInstFileName = RemottoApp.Global.exedirpath + $"\\{InstFileName}.tmp";
        }

        public void setMainWindow(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
            this.mainWindow.Opacity = 0.4;
           
            
        }

        private void FinishedBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            this.mainWindow.Opacity = 1;
        }

        public void connectState()
        {
            registerPanel.Visibility = Visibility.Collapsed;
            bluetoothPanel.Visibility = Visibility.Visible;
            exit.Visibility = Visibility.Visible;
        }

        private void textBox_TextChanged(object sender, RoutedEventArgs e)
        {
            if (TextBoxFullName.Text.Length > 0 && TextBoxGamerName.Text.Length > 0 && TextBoxEmail.Text.Length > 0)
            {
                nextBtn.IsEnabled = true;
            }
            else
            {
                nextBtn.IsEnabled = false;

            }
        }

            private void nextBtn_Click(object sender, RoutedEventArgs e)
        {
            if (registerPanel.Visibility == Visibility.Visible)
            {
                try
                {
                    MailAddress m = new MailAddress(TextBoxEmail.Text);

                    registerPanel.Visibility = Visibility.Collapsed;
                    bluetoothPanel.Visibility = Visibility.Visible;
                    exit.Visibility = Visibility.Visible;

                    Properties.Settings.Default.UserName = TextBoxGamerName.Text;
                    Properties.Settings.Default.Save();
                    mainWindow.gamersProfile.editSelfName(TextBoxGamerName.Text);

                    string userSettingsString = Properties.Settings.Default.userID.ToString() + ", " + TextBoxFullName.Text + ", " + TextBoxGamerName.Text + ", " + TextBoxEmail.Text;
                    var page = mainWindow.MainAnalyticsSession.CreatePageViewRequest("/", "RegisterPopup");
                    page.SendEvent("register", "register", userSettingsString, "1");

                    
                }
                catch (FormatException)
                {
                    Validated.Visibility = Visibility.Visible;
                }
                
            }
            else
            {
                bluetoothPanel.Visibility = Visibility.Collapsed;
                pairingPanel.Visibility = Visibility.Visible;
            }
        }

        private void VigemInstallBtn_Click(object sender, RoutedEventArgs e)
        {
            if (File.Exists(RemottoApp.Global.exedirpath + $"\\{InstFileName}"))
            {
                File.Delete(RemottoApp.Global.exedirpath + $"\\{InstFileName}");
            }

            if (File.Exists(tempInstFileName))
            {
                 File.Delete(tempInstFileName);
            }

            ViGEmDownloadLaunch();

            /*WebClient wb = new WebClient();
            wb.DownloadFileAsync(new Uri(InstallerDL), exepath + $"\\{InstFileName}");

            wb.DownloadProgressChanged += wb_DownloadProgressChanged;
            wb.DownloadFileCompleted += wb_DownloadFileCompleted;
            */
        }

        private async void ViGEmDownloadLaunch()
        {
            Progress<ICopyProgress> progress = new Progress<ICopyProgress>(x => // Please see "Notes on IProgress<T>"
            {
                // This is your progress event!
                // It will fire on every buffer fill so don't do anything expensive.
                // Writing to the console IS expensive, so don't do the following in practice...
                vigemInstallBtn.Content = Translations.Strings.Downloading.Replace("*number*%",
                    x.PercentComplete.ToString("P"));
                //Console.WriteLine(x.PercentComplete.ToString("P"));
            });

            string filename = RemottoApp.Global.exedirpath + $"\\{InstFileName}";
            bool success = false;
            using (var downloadStream = new FileStream(tempInstFileName, FileMode.CreateNew))
            {
                HttpResponseMessage response = await App.requestClient.GetAsync(InstallerDL,
                    downloadStream, progress);
                success = response.IsSuccessStatusCode;
            }

            if (success)
            {
                File.Move(tempInstFileName, filename);
            }
            success = false; // Reset for later check

            if (File.Exists(RemottoApp.Global.exedirpath + $"\\{InstFileName}"))
            {
                //vigemInstallBtn.Content = Properties.Resources.OpeningInstaller;
                monitorProc = Process.Start(RemottoApp.Global.exedirpath + $"\\{InstFileName}");
                vigemInstallBtn.Content = Translations.Strings.Installing;
                success = true;
            }

            if (success)
            {
                monitorTimer = new NonFormTimer();
                monitorTimer.Elapsed += ViGEmInstallTimer_Tick;
                monitorTimer.Start();
            }
            else
            {
                vigemInstallBtn.Content = Properties.Resources.InstallFailed;
            }
        }

        private void ViGEmInstallTimer_Tick(object sender, System.Timers.ElapsedEventArgs e)
        {
            ((NonFormTimer)sender).Stop();
            bool finished = false;
            if (monitorProc != null && monitorProc.HasExited)
            {
                if (RemottoApp.Global.IsViGEmBusInstalled())
                {
                    Dispatcher.BeginInvoke((Action)(() => { vigemInstallBtn.Content = Translations.Strings.InstallComplete; }));
                }
                else
                {
                    Dispatcher.BeginInvoke((Action)(() => { vigemInstallBtn.Content = Properties.Resources.InstallFailed; }), null);
                }

                File.Delete(RemottoApp.Global.exedirpath + $"\\{InstFileName}");
                ((NonFormTimer)sender).Stop();
                finished = true;
            }

            if (!finished)
            {
                ((NonFormTimer)sender).Start();
            }
        }

        private void Step2Btn_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("http://www.microsoft.com/accessories/en-gb/d/xbox-360-controller-for-windows");
        }

        private void RemottoBuy(object sender, RoutedEventArgs e)
        {
            RequestNavigateEventArgs navigate = new RequestNavigateEventArgs(new Uri("https://es.remottobattery.com/"),"www.remottobattery.com");
            //navigate.Uri("www.remottobattery.com");
            System.Diagnostics.Process.Start("https://remottobattery.com/products/remotto-bluetooth-adapter");
        }

        private void BluetoothSetLink_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("control", "bthprops.cpl");
        }

        private void BTdevice_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(e.Uri.AbsoluteUri);
        }

        private void title_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }
}
