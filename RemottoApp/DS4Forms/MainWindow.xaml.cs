﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.Windows.Interop;
using System.Diagnostics;
using System.IO;
using System.Management;
using NonFormTimer = System.Timers.Timer;
using System.Runtime.InteropServices;
using RemottoApp.DS4Forms.ViewModels;
using HttpProgress;
using System.Windows.Controls.Primitives;
using Windows.Storage;
using Garlic;
using System.Net.Http;
using System.Collections;
using Microsoft.Toolkit.Uwp.Notifications;
using Microsoft.Toolkit;

namespace RemottoApp.DS4Forms
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    [System.Security.SuppressUnmanagedCodeSecurity]
    public partial class MainWindow : Window
    {
        private MainWindowsViewModel mainWinVM;
        private StatusLogMsg lastLogMsg = new StatusLogMsg();
        private ProfileList profileListHolder = new ProfileList();
        private LogViewModel logvm;
        private YoutuberListViewModel youListViewModel;
        private ControllerListViewModel conLvViewModel;
        private TrayIconViewModel trayIconVM;
        private SettingsViewModel settingsWrapVM;
        private ExitWindow exitWindow;
        private IntPtr regHandle = new IntPtr();
        private bool showAppInTaskbar = false;
        bool maxormin = false;
        private ManagementEventWatcher managementEvWatcher;
        private bool wasrunning = false;
        private AutoProfileHolder autoProfileHolder;
        private NonFormTimer hotkeysTimer;
        private NonFormTimer autoProfilesTimer;
        private AutoProfileChecker autoprofileChecker;
        private ProfileEditor editor;
        private bool preserveSize = true;
        private Size oldSize;
        private bool contextclose;
        public int screenTag;
        private bool needsUpdate;
        private AnalyticsSession analyticsSession;

        public AnalyticsSession MainAnalyticsSession { get => analyticsSession; }

        public ProfileList ProfileListHolder { get => profileListHolder; }

        public MainWindow(ArgumentParser parser)
        {
            InitializeComponent();

            mainWinVM = new MainWindowsViewModel();
            DataContext = mainWinVM;
            App root = Application.Current as App;
            settingsWrapVM = new SettingsViewModel();
            logvm = new LogViewModel(App.rootHub);

            profileListHolder.Refresh();

            conLvViewModel = new ControllerListViewModel(App.rootHub, profileListHolder);
            youListViewModel = new YoutuberListViewModel();
            controllers.DataContext = conLvViewModel;
            // youtubersList.DataContext = youListViewModel;
            responsiveInit();
            gamersProfile.UpdateYoutubersViewModel(youListViewModel);
            gamersProfile.UpdateLvViewModel(conLvViewModel);
            gamersProfile.setupDataContext();
            gamersProfile.setMainWindow(this);
            mainTabCon.SelectedIndex = 1;

            trayIconVM = new TrayIconViewModel(App.rootHub, profileListHolder);
            notifyIcon.DataContext = trayIconVM;

            if (Global.StartMinimized || parser.Mini)
            {
                WindowState = WindowState.Minimized;
            }

            bool isElevated = Global.IsAdministrator();
            /*if (isElevated)
            {
                uacImg.Visibility = Visibility.Collapsed;
            }*/

            this.Width = Global.FormWidth;
            this.Height = Global.FormHeight;
            //WindowStartupLocation = WindowStartupLocation.Manual;
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.Left = Global.FormLocationX;
            this.Top = Global.FormLocationY;
            maxormin = true;
            autoProfileHolder = autoProfControl.AutoProfileHolder;
            autoProfControl.SetupDataContext(profileListHolder);
            autoProfControl.LauncherMenuItem_Click(autoProfControl.launcherMenuItem, new RoutedEventArgs(ButtonBase.ClickEvent));
            
            //conLvViewModel.setProgramColl(autoProfControl.AutoProfVM.ProgramColl);
            //controllerGamesList.DataContext = autoProfControl.AutoProfVM;
            //controllerGamesList.ItemsSource = autoProfControl.AutoProfVM.ProgramColl;
            //SwitchView = 0
            autoprofileChecker = new AutoProfileChecker(autoProfileHolder);

            SetupEvents();

            //check if BTH WIN are installed
        /*    try
            {
                string ComputerName = "localhost";
                ManagementScope Scope;
                Scope = new ManagementScope(String.Format("\\\\{0}\\root\\CIMV2", ComputerName), null);

                Scope.Connect();
                ObjectQuery Query = new ObjectQuery("SELECT * FROM Win32_PnPSignedDriver");
                ManagementObjectSearcher Searcher = new ManagementObjectSearcher(Scope, Query);
                Queue deviceName = new Queue();
                Queue bthfound = new Queue();
                foreach (ManagementObject WmiObject in Searcher.Get())
                {
                    Console.WriteLine("{0,-35} {1,-40}", "ClassGuid", WmiObject["ClassGuid"]);// String
                    Console.WriteLine("{0,-35} {1,-40}", "DeviceClass", WmiObject["DeviceClass"]);// String
                    Console.WriteLine("{0,-35} {1,-40}", "DeviceID", WmiObject["DeviceID"]);// String
                    Console.WriteLine("{0,-35} {1,-40}", "DeviceName", WmiObject["DeviceName"]);// String
                    Console.WriteLine("{0,-35} {1,-40}", "Manufacturer", WmiObject["Manufacturer"]);// String
                    Console.WriteLine("{0,-35} {1,-40}", "Name", WmiObject["Name"]);// String
                    Console.WriteLine("{0,-35} {1,-40}", "Status", WmiObject["Status"]);// String

                    if(WmiObject != null || WmiObject.GetType() != typeof(int))
                    {
                        try
                        {
                            deviceName.Enqueue(WmiObject["DeviceName"].ToString());
                            if (WmiObject["DeviceName"].ToString().Contains("Bluetooth"))
                            {
                                bthfound.Enqueue(WmiObject["DeviceName"].ToString());
                                Console.WriteLine("Exist Bluetooth");

                            }
                        }
                        catch(Exception e)
                        {
                            //Excep
                        }
                       
                    }
                    
                }
                bthfound.Clear();
                if(bthfound.Count == 0)
                {
                    string[] devices = { "BTH\\MS_BTHPAN", "BTHENUM" };
                    // function to install BTH Drivers!!!
                    string infbthpath = "C:\\Users\\Juan\\Pictures\\bth\\bth.inf";
                    InstallHinfSectionW(IntPtr.Zero, IntPtr.Zero, infbthpath, 0);
                    InstallBTHDrivers(infbthpath, devices);
                }         
            }
              catch (Exception e)
            {
                Console.WriteLine(String.Format("Exception {0} Trace {1}", e.Message, e.StackTrace));
            }
        */
            

            analyticsSession = new AnalyticsSession("remottopcapp.com", "UA-73168939-4");

            if (Properties.Settings.Default.visitCount == 0)
            {
                Random r = new Random();
                int rInt = r.Next(1, 100000000);
                Properties.Settings.Default.userID = rInt;
                Properties.Settings.Default.visitCount += 1;
                Properties.Settings.Default.Save();

                string userIDString = Properties.Settings.Default.userID.ToString();
                string sessionCount = Properties.Settings.Default.visitCount.ToString();
                analyticsSession.SetCustomVariable(1, "user_id", userIDString);
                analyticsSession.SetCustomVariable(2, "session_count", sessionCount);

                var page = analyticsSession.CreatePageViewRequest("/", "Main");
                page.SendEvent("install", "install", userIDString, "1");

                InitialGrid.Visibility = Visibility.Visible;
                MainGrid.Visibility = Visibility.Collapsed;
                mainTabCon.Visibility = Visibility.Collapsed;
            }
            else
            {
                InitialGrid.Visibility = Visibility.Collapsed;
                MainGrid.Visibility = Visibility.Visible;
                mainTabCon.Visibility = Visibility.Visible;

                Properties.Settings.Default.visitCount += 1;
                Properties.Settings.Default.Save();
            }

            string userIDString2 = Properties.Settings.Default.userID.ToString();
            string sessionCount2 = Properties.Settings.Default.visitCount.ToString();
            analyticsSession.SetCustomVariable(1, "user_id", userIDString2);
            analyticsSession.SetCustomVariable(2, "session_count", sessionCount2);

            var page2 = analyticsSession.CreatePageViewRequest("/", "Main");
            page2.SendEvent("session", "session", userIDString2, "1");

            Thread timerThread = new Thread(() =>
            {
                hotkeysTimer = new NonFormTimer();
                hotkeysTimer.Interval = 20;
                hotkeysTimer.AutoReset = false;

                autoProfilesTimer = new NonFormTimer();
                autoProfilesTimer.Interval = 1000;
                autoProfilesTimer.AutoReset = false;
            });
            timerThread.IsBackground = true;
            timerThread.Priority = ThreadPriority.Lowest;
            timerThread.Start();
            timerThread.Join();

            Application.Current.MainWindow.Height = 725;
            Application.Current.MainWindow.Width = 1375;
            Application.Current.MainWindow.Left = 0;
            Application.Current.MainWindow.Top = 0;
        }

        public class TodoItem
        {
            public string Title { get; set; }
            public int Completion { get; set; }
        }

        public void SendMailAnalytics(object sender, RoutedEventArgs e)
        {

            
            //analyticsSession = new AnalyticsSession("remottopcapp.com", "UA-73168939-4");

            analyticsSession.SetCustomVariable(3, "keepmeupdated", "help@remottobattery.com");

            var page = analyticsSession.CreatePageViewRequest("/", "Main");
            page.SendEvent("keepmeupdated", "keepmeupdated", "john@doe.com", "1");



        }

        private void SendMailKeepMeUpdated(object sender, RoutedEventArgs e)
        {

            KeepMeUpdated keepMeUpdated = new KeepMeUpdated();

            //keepMeUpdated = this;
            //keepMeUpdated.setMainWindow(this);
            //wdiag.connectState();
            keepMeUpdated.Show();
            keepMeUpdated.setMainWindow(this);         


        }



        public void LateChecks(ArgumentParser parser)
        {
            Task tempTask = Task.Run(() =>
            {
                CheckDrivers();
                if (!parser.Stop)
                {
                    Dispatcher.BeginInvoke((Action)(() =>
                    {
                        //StartStopBtn.IsEnabled = false;
                    }));
                    Thread.Sleep(500);
                    App.rootHub.Start();
                    conLvViewModel.setPrintingController(0);
                    //root.rootHubtest.Start();
                }

                //UpdateTheUpdater();
            });

            Util.LogAssistBackgroundTask(tempTask);

            tempTask = Task.Delay(100).ContinueWith((t) =>
            {
                int checkwhen = Global.CheckWhen;

                string version = RemottoApp.Global.exeversion;

                needsUpdate = needUpdate(version);
                if (needsUpdate)
                {
                    Application.Current.Dispatcher.Invoke((Action)delegate {
                        UpdateVersion updateVer = new UpdateVersion();
                        updateVer.setMainWindow(this);
                        updateVer.ShowDialog();
                    });
                    CheckAndUpdate();
                }
                else if (checkwhen > 0 && DateTime.Now >= Global.LastChecked + TimeSpan.FromHours(checkwhen))
                {
                    CheckAndUpdate();
                    Global.LastChecked = DateTime.Now;
                }
            });
            Util.LogAssistBackgroundTask(tempTask);
        }

        private Boolean needUpdate(string version)
        {
            //string minimumVer = RemottoApp.Properties.Settings.Default.minVersion;

            Uri url = new Uri("https://bitbucket.org/remotto-tech/remotto-ds4-app/raw/master/RemottoApp/minimum_version.txt");

            HttpResponseMessage response = App.requestClient.GetAsync(url.ToString()).Result;
            if (response.IsSuccessStatusCode)
            {
                string minimumVer = response.Content.ReadAsStringAsync().Result.Trim();

                Version minVer = new Version(minimumVer);
                Version currVer = new Version(version);

                if (currVer.CompareTo(minVer) < 0)
                {
                    return true;
                }
            }

            return false;
        }

        private void DownloadUpstreamVersionInfo()
        {
            // Sorry other devs, gonna have to find your own server
            Uri url = new Uri("https://bitbucket.org/remotto-tech/remotto-ds4-app/raw/master/RemottoApp/newest.txt");
            string filename = Global.appdatapath + "\\version.txt";
            using (var downloadStream = new FileStream(filename, FileMode.Create))
            {
                Task<System.Net.Http.HttpResponseMessage> temp = App.requestClient.GetAsync(url.ToString(), downloadStream);
                temp.Wait();
            }
        }

        public void CheckAndUpdate()
        {
            DownloadUpstreamVersionInfo();
            Check_Version();
        }

        private string DownloadUpstreamUpdaterVersion()
        {
            string result = string.Empty;
            // Sorry other devs, gonna have to find your own server
            Uri url = new Uri("https://raw.githubusercontent.com/Ryochan7/DS4Updater/master/Updater2/newest.txt");
            string filename = System.IO.Path.Combine(System.IO.Path.GetTempPath(), "DS4Updater_version.txt");
            bool readFile = false;
            using (var downloadStream = new FileStream(filename, FileMode.Create))
            {
                Task<System.Net.Http.HttpResponseMessage> temp = App.requestClient.GetAsync(url.ToString(), downloadStream);
                temp.Wait();

                if (temp.Result.IsSuccessStatusCode) readFile = true;
            }

            if (readFile)
            {
                result = File.ReadAllText(filename).Trim();
                File.Delete(filename);
            }

            return result;
        }

        private void Start_Btn_Click(object sender, RoutedEventArgs e)
        {
            InitialGrid.Visibility = Visibility.Collapsed;
            MainGrid.Visibility = Visibility.Visible;
            mainTabCon.Visibility = Visibility.Visible;
            Application.Current.MainWindow.Height = 725;
            Application.Current.MainWindow.Width = 1375;
            gamersProfile.userDataPopup(sender, e);
        }

        private void Check_Version(bool showstatus = false)
        {
            string version = Global.exeversion;
            string newversion = File.ReadAllText(Global.appdatapath + "\\version.txt").Trim();
            if (!string.IsNullOrWhiteSpace(newversion) && version.CompareTo(newversion) != 0)
            {
                MessageBoxResult result = MessageBoxResult.No;
                Dispatcher.Invoke(() =>
                {
                    result = MessageBox.Show(Properties.Resources.DownloadVersion.Replace("*number*", newversion),
                                             Properties.Resources.DS4Update, MessageBoxButton.YesNo, MessageBoxImage.Question);
                });

                if (result == MessageBoxResult.Yes)
                {
                    bool launch = true;
                    launch = RunUpdaterCheck(launch);

                    if (launch)
                    {
                        using (Process p = new Process())
                        {
                            p.StartInfo.FileName = System.IO.Path.Combine(Global.exedirpath, "RemottoAppUpdater.exe");
                            bool isAdmin = Global.IsAdministrator();
                            List<string> argList = new List<string>();
                            argList.Add("-autolaunch");
                            if (!isAdmin)
                            {
                                argList.Add("-user");
                            }
                            p.StartInfo.Arguments = string.Join(" ", argList);
                            if (Global.AdminNeeded())
                                p.StartInfo.Verb = "runas";

                            try { launch = p.Start(); }
                            catch (InvalidOperationException) { }
                        }
                    }

                    if (launch)
                    {
                        Dispatcher.BeginInvoke((Action)(() =>
                        {
                            contextclose = true;
                            Close();
                        }));
                    }
                    else
                    {
                        Dispatcher.Invoke(() =>
                        {
                            MessageBox.Show(Properties.Resources.PleaseDownloadUpdater);
                        });
                        //Process.Start($"https://github.com/Ryochan7/DS4Updater/releases/download/v{UPDATER_VERSION}/{updaterExe}");
                    }
                }
                else
                {
                    File.Delete(Global.appdatapath + "\\version.txt");
                    if (needsUpdate)
                    {
                        Environment.Exit(0);
                    }
                }
            }
            else
            {
                File.Delete(Global.appdatapath + "\\version.txt");
                if (showstatus)
                {
                    Dispatcher.Invoke(() => MessageBox.Show(Properties.Resources.UpToDate, "DS4Windows Updater"));
                }
            }
        }

        private void ScrollViewer_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            ScrollViewer scrollviewer = sender as ScrollViewer;
            if (e.Delta > 0)
                scrollviewer.LineLeft();
            else
                scrollviewer.LineRight();
            e.Handled = true;
        }

        private void ProjectListView_OnRequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
        {
            e.Handled = true;
        }
        private bool RunUpdaterCheck(bool launch)
        {
            string destPath = Global.exedirpath + "\\RemottoAppUpdater.exe";
            bool updaterExists = File.Exists(destPath);
            //string version = DownloadUpstreamUpdaterVersion();
            string version = "1.0.0";
            if (!updaterExists ||
                (!string.IsNullOrEmpty(version) && FileVersionInfo.GetVersionInfo(destPath).FileVersion.CompareTo(version) != 0))
            {
                launch = false;
                //Uri url2 = new Uri($"https://github.com/Ryochan7/DS4Updater/releases/download/v{version}/{mainWinVM.updaterExe}");
                Uri url2 = new Uri($"https://bitbucket.org/remotto-tech/remottoappupdater/downloads/RemottoAppUpdater.exe");
            
                string filename = System.IO.Path.Combine(System.IO.Path.GetTempPath(), "RemottoAppUpdater.exe");
                using (var downloadStream = new FileStream(filename, FileMode.Create))
                {
                    Task<System.Net.Http.HttpResponseMessage> temp =
                        App.requestClient.GetAsync(url2.ToString(), downloadStream);
                    temp.Wait();
                    if (temp.Result.IsSuccessStatusCode) launch = true;
                }

                if (launch)
                {
                    if (Global.AdminNeeded())
                    {
                        int copyStatus = Util.ElevatedCopyUpdater(filename);
                        if (copyStatus != 0) launch = false;
                    }
                    else
                    {
                        if (updaterExists) File.Delete(destPath);
                        File.Move(filename, destPath);
                    }
                }
            }

            return launch;
        }

        private void TrayIconVM_RequestMinimize(object sender, EventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void TrayIconVM_ProfileSelected(TrayIconViewModel sender,
            ControllerHolder item, string profile)
        {
            int idx = item.Index;
            CompositeDeviceModel devitem = conLvViewModel.ControllerDict[idx];
            if (devitem != null)
            {
                devitem.ChangeSelectedProfile(profile);
            }
        }

        private void ShowNotification(object sender, RemottoApp.DebugEventArgs e)
        {
            Dispatcher.BeginInvoke((Action)(() =>
            {

                /* if (!IsActive && (Global.Notifications == 2 ||
                     (Global.Notifications == 1 && e.Warning)))
                 {
                     notifyIcon.ShowBalloonTip(TrayIconViewModel.ballonTitle,
                     e.Data, !e.Warning ? Hardcodet.Wpf.TaskbarNotification.BalloonIcon.Info :
                     Hardcodet.Wpf.TaskbarNotification.BalloonIcon.Warning);
                 }*/

                new ToastContentBuilder()
                .AddText("Remotto App")
                .AddText(e.Data)
                .Show();
                    
            



            }));
        }

        private void SetupEvents()
        {
            App root = Application.Current as App;
            App.rootHub.ServiceStarted += ControlServiceStarted;
            App.rootHub.RunningChanged += ControlServiceChanged;
            App.rootHub.PreServiceStop += PrepareForServiceStop;
            //root.rootHubtest.RunningChanged += ControlServiceChanged;
            conLvViewModel.ControllerCol.CollectionChanged += ControllerCol_CollectionChanged;
            AppLogger.TrayIconLog += ShowNotification;
            AppLogger.GuiLog += UpdateLastStatusMessage;
            //logvm.LogItems.CollectionChanged += LogItems_CollectionChanged;
            App.rootHub.Debug += UpdateLastStatusMessage;
            trayIconVM.RequestShutdown += TrayIconVM_RequestShutdown;
            trayIconVM.ProfileSelected += TrayIconVM_ProfileSelected;
            trayIconVM.RequestMinimize += TrayIconVM_RequestMinimize;
            trayIconVM.RequestOpen += TrayIconVM_RequestOpen;         
            trayIconVM.RequestServiceChange += TrayIconVM_RequestServiceChange;
            autoProfControl.AutoDebugChanged += AutoProfControl_AutoDebugChanged;
            autoprofileChecker.RequestServiceChange += AutoprofileChecker_RequestServiceChange;
            autoProfileHolder.AutoProfileColl.CollectionChanged += AutoProfileColl_CollectionChanged;
            //autoProfControl.AutoProfVM.AutoProfileSystemChange += AutoProfVM_AutoProfileSystemChange;
            mainWinVM.FullTabsEnabledChanged += MainWinVM_FullTabsEnabledChanged;

            bool wmiConnected = false;
            WqlEventQuery q = new WqlEventQuery();
            ManagementScope scope = new ManagementScope("root\\CIMV2");
            q.EventClassName = "Win32_PowerManagementEvent";

            try
            {
                scope.Connect();
            }
            catch (COMException) { }

            if (scope.IsConnected)
            {
                wmiConnected = true;
                managementEvWatcher = new ManagementEventWatcher(scope, q);
                managementEvWatcher.EventArrived += PowerEventArrive;
                try
                {
                    managementEvWatcher.Start();
                }
                catch (ManagementException) { wmiConnected = false; }
            }

            if (!wmiConnected)
            {
                AppLogger.LogToGui(@"Could not connect to Windows Management Instrumentation service.
                                   Suspend support not enabled.", true);
            }
        }

        private void MainWinVM_FullTabsEnabledChanged(object sender, EventArgs e)
        {
            settingsWrapVM.ViewEnabled = mainWinVM.FullTabsEnabled;
        }

        private void TrayIconVM_RequestServiceChange(object sender, EventArgs e)
        {
            ChangeService();
        }

        /*private void LogItems_CollectionChanged(object sender,
            System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                Dispatcher.BeginInvoke((Action)(() =>
                {
                    int count = logListView.Items.Count;
                    if (count > 0)
                    {
                        logListView.ScrollIntoView(logvm.LogItems[count - 1]);
                    }
                }));
            }
        }*/

        private void ControlServiceStarted(object sender, EventArgs e)
        {
            if (Global.SwipeProfiles)
            {
                ChangeHotkeysStatus(true);
            }

            CheckAutoProfileStatus();
        }

        private void AutoprofileChecker_RequestServiceChange(AutoProfileChecker sender, bool state)
        {
            Dispatcher.BeginInvoke((Action)(() =>
            {
                ChangeService();
            }));
        }

        private void AutoProfVM_AutoProfileSystemChange(AutoProfilesViewModel sender, bool state)
        {
            if (state)
            {
                ChangeAutoProfilesStatus(true);
                autoProfileHolder.AutoProfileColl.CollectionChanged += AutoProfileColl_CollectionChanged;
            }
            else
            {
                ChangeAutoProfilesStatus(false);
                autoProfileHolder.AutoProfileColl.CollectionChanged -= AutoProfileColl_CollectionChanged;
            }
        }

        private void AutoProfileColl_CollectionChanged(object sender,
            System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            CheckAutoProfileStatus();
        }

        private void AutoProfControl_AutoDebugChanged(object sender, EventArgs e)
        {
            autoprofileChecker.AutoProfileDebugLogLevel = autoProfControl.AutoDebug == true ? 1 : 0;
        }

        private void PowerEventArrive(object sender, EventArrivedEventArgs e)
        {
            short evType = Convert.ToInt16(e.NewEvent.GetPropertyValue("EventType"));
            switch (evType)
            {
                // Wakeup from Suspend
                case 7:
                    DS4LightBar.shuttingdown = false;
                    App.rootHub.suspending = false;

                    if (wasrunning)
                    {
                        wasrunning = false;
                        Thread.Sleep(16000);
                        Dispatcher.BeginInvoke((Action)(() =>
                        {
                            //StartStopBtn.IsEnabled = false;
                        }));

                        App.rootHub.Start();
                    }

                    break;
                // Entering Suspend
                case 4:
                    DS4LightBar.shuttingdown = true;
                    Program.rootHub.suspending = true;

                    if (App.rootHub.running)
                    {
                        Dispatcher.BeginInvoke((Action)(() =>
                        {
                            //StartStopBtn.IsEnabled = false;
                        }));

                        App.rootHub.Stop();
                        wasrunning = true;
                    }

                    break;

                default: break;
            }
        }

        private void UpdateTheUpdater()
        {
            if (File.Exists(Global.exedirpath + "\\Update Files\\DS4Windows\\DS4Updater.exe"))
            {
                Process[] processes = Process.GetProcessesByName("DS4Updater");
                while (processes.Length > 0)
                {
                    Thread.Sleep(500);
                    processes = Process.GetProcessesByName("DS4Updater");
                }

                if (!Global.AdminNeeded())
                {
                    File.Delete(Global.exedirpath + "\\DS4Updater.exe");
                    File.Move(Global.exedirpath + "\\Update Files\\DS4Windows\\DS4Updater.exe",
                        Global.exedirpath + "\\DS4Updater.exe");
                    Directory.Delete(Global.exedirpath + "\\Update Files", true);
                }
                else
                {
                    Util.ElevatedCopyUpdater(Global.exedirpath + "\\Update Files\\DS4Windows\\DS4Updater.exe", true);
                }
            }
        }

        private void ChangeHotkeysStatus(bool state)
        {
            if (state)
            {
                hotkeysTimer.Elapsed += HotkeysTimer_Elapsed;
                hotkeysTimer.Start();
            }
            else
            {
                hotkeysTimer.Stop();
                hotkeysTimer.Elapsed -= HotkeysTimer_Elapsed;
            }
        }

        private void HotkeysTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            hotkeysTimer.Stop();

            if (Global.SwipeProfiles)
            {
                foreach (CompositeDeviceModel item in conLvViewModel.ControllerCol)
                //for (int i = 0; i < 4; i++)
                {
                    string slide = App.rootHub.TouchpadSlide(item.DevIndex);
                    if (slide == "left")
                    {
                        //int ind = i;
                        Dispatcher.BeginInvoke((Action)(() =>
                        {
                            if (item.SelectedIndex <= 0)
                            {
                                item.SelectedIndex = item.ProfileListCol.Count - 1;
                            }
                            else
                            {
                                item.SelectedIndex--;
                            }
                        }));
                    }
                    else if (slide == "right")
                    {
                        //int ind = i;
                        Dispatcher.BeginInvoke((Action)(() =>
                        {
                            if (item.SelectedIndex == (item.ProfileListCol.Count - 1))
                            {
                                item.SelectedIndex = 0;
                            }
                            else
                            {
                                item.SelectedIndex++;
                            }
                        }));
                    }

                    if (slide.Contains("t"))
                    {
                        //int ind = i;
                        Dispatcher.BeginInvoke((Action)(() =>
                        {
                            string temp = Properties.Resources.UsingProfile.Replace("*number*",
                                (item.DevIndex + 1).ToString()).Replace("*Profile name*", item.SelectedProfile);
                            ShowHotkeyNotification(temp);
                        }));
                    }
                }
            }

            hotkeysTimer.Start();
        }

        private void ShowHotkeyNotification(string message)
        {
            if (!IsActive && (Global.Notifications == 2))
            {
                notifyIcon.ShowBalloonTip(TrayIconViewModel.ballonTitle,
                message, Hardcodet.Wpf.TaskbarNotification.BalloonIcon.Info);
            }
        }

        private void PrepareForServiceStop(object sender, EventArgs e)
        {
            Dispatcher.BeginInvoke((Action)(() =>
            {
                trayIconVM.ClearContextMenu();
            }));

            ChangeHotkeysStatus(false);
        }

        private void TrayIconVM_RequestOpen(object sender, EventArgs e)
        {
            if (!showAppInTaskbar)
            {
                Show();
            }

            maxormin = true;
            WindowState = WindowState.Maximized;
          
        }
        private void responsiveInit()
        {
            double widthOriginal = System.Windows.SystemParameters.PrimaryScreenWidth;
            double heigthOriginal = System.Windows.SystemParameters.PrimaryScreenHeight;

            if (widthOriginal >= 1920 && heigthOriginal >= 1080)
            {
                //gridController.Margin = new Thickness(0, 110, 0, 0);
                asset2.Width = 1000;
                asset2.Height = 560;
            }
            else
            {
                //gridController.Margin = new Thickness(0, 10, 0, 0);
                asset2.Width = 1000;
                asset2.Height = 460;
            }







        }

        private void TrayIconVM_RequestShutdown(object sender, EventArgs e)
        {
                      
            if (conLvViewModel.ControllerCol.Count > 0)
            {
                if (maxormin == false)
                {
                    Activate();
                    Show();
                    maxormin = true;
                    bool result = false;
                    ExitWindow test = new ExitWindow();
                    test.Show();
                    test.setMainWindow(this.mainDS4Window);

                    

                    /*MessageBoxResult result = MessageBox.Show(Translations.Strings.Close, Translations.Strings.Confirm,
                            MessageBoxButton.YesNo, MessageBoxImage.Question);*/
          


                    /*if (result == MessageBoxResult.No)
                    {                       
                        return;
                    }
                    else
                    {
                        contextclose = true;
                        this.Close();
                    }*/
                }
                else
                {
                    //Show();
                    Activate();
                    maxormin = false;
                    ExitWindow test = new ExitWindow();
                    test.Show();
                    test.setMainWindow(this.mainDS4Window);



                    /*MessageBoxResult result2 = MessageBox.Show(Translations.Strings.Close, Translations.Strings.Confirm,
                            MessageBoxButton.YesNo, MessageBoxImage.Question);*/
                    /*if (result2 == MessageBoxResult.No)
                    {
                        return;
                    }
                    else
                    {
                        contextclose = true;
                        this.Close();
                    }*/
                }

            }
            else
            {
                contextclose = true;
                this.Close();
            }
        
        }

        private void UpdateLastStatusMessage(object sender, RemottoApp.DebugEventArgs e)
        {
            lastLogMsg.Message = e.Data;
            lastLogMsg.Warning = e.Warning;
        }

        private void ChangeAutoProfilesStatus(bool state)
        {
            if (state)
            {
                autoProfilesTimer.Elapsed += AutoProfilesTimer_Elapsed;
                autoProfilesTimer.Start();
                autoprofileChecker.Running = true;
            }
            else
            {
                autoProfilesTimer.Stop();
                autoProfilesTimer.Elapsed -= AutoProfilesTimer_Elapsed;
                autoprofileChecker.Running = false;
            }
        }

        private void CheckAutoProfileStatus()
        {
            int pathCount = autoProfileHolder.AutoProfileColl.Count;
            bool timerEnabled = autoprofileChecker.Running;
            if (pathCount > 0 && !timerEnabled)
            {
                ChangeAutoProfilesStatus(true);
            }
            else if (pathCount == 0 && timerEnabled)
            {
                ChangeAutoProfilesStatus(false);
            }
        }

        private void AutoProfilesTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            autoProfilesTimer.Stop();
            //Console.WriteLine("Event triggered");
            autoprofileChecker.Process();

            if (autoprofileChecker.Running)
            {
                autoProfilesTimer.Start();
            }
        }

        private void ControllerCol_CollectionChanged(object sender,
            System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            Dispatcher.BeginInvoke((Action)(() =>
            {
                //ChangeControllerPanel();
                System.Collections.IList newitems = e.NewItems;
                if (newitems != null)
                {
                    foreach (CompositeDeviceModel item in newitems)
                    {
                        item.LightContext = new ContextMenu();
                        item.AddLightContextItems();
                        item.Device.SyncChange += DS4Device_SyncChange;
                        item.RequestColorPicker += Item_RequestColorPicker;
                        //item.LightContext.Items.Add(new MenuItem() { Header = "Use Profile Color", IsChecked = !item.UseCustomColor });
                        //item.LightContext.Items.Add(new MenuItem() { Header = "Use Custom Color", IsChecked = item.UseCustomColor });
                    }

                    conLvViewModel.setPrintingController(0);

                    analyticsSession = new AnalyticsSession("remottopcapp.com", "UA-73168939-4");
                    //analyticsSession = new AnalyticsSession("remottopcapp.com", "UA-73168939-4");
                    analyticsSession.SetCustomVariable(4, "linkedcontroller", Properties.Settings.Default.userID.ToString());
                    var page = analyticsSession.CreatePageViewRequest("/", "Main");
                    page.SendEvent("linkedcontroller", "linkedcontroller", Properties.Settings.Default.userID.ToString(), "1");

                }

                gamersProfile.UpdateLvViewModel(conLvViewModel);
               

                if (App.rootHub.running)
                    trayIconVM.PopulateContextMenu();

            }));
        }

        private void Item_RequestColorPicker(CompositeDeviceModel sender)
        {
            ColorPickerWindow dialog = new ColorPickerWindow();
            dialog.Owner = this;
            dialog.colorPicker.SelectedColor = sender.CustomLightColor;
            dialog.ColorChanged += (sender2, color) =>
            {
                sender.UpdateCustomLightColor(color);
            };
            dialog.ShowDialog();
        }

        private void DS4Device_SyncChange(object sender, EventArgs e)
        {
            Dispatcher.BeginInvoke((Action)(() =>
            {
                trayIconVM.PopulateContextMenu();
            }));
        }

        private void ControlServiceChanged(object sender, EventArgs e)
        {
            //Tester service = sender as Tester;
            ControlService service = sender as ControlService;
            Dispatcher.BeginInvoke((Action)(() =>
            {
                if (service.running)
                {
                    //StartStopBtn.Content = Translations.Strings.StopText;
                }
                else
                {
                    //StartStopBtn.Content = Translations.Strings.StartText;
                }

                //StartStopBtn.IsEnabled = true;
            }));
        }

        private void helpmeBtn_Click(object sender, RoutedEventArgs e)
        {
            PopUp wdiag = new PopUp();
            wdiag.Owner = this;
            wdiag.setMainWindow(this);
            wdiag.connectState();
            wdiag.ShowDialog();
        }

        private void AboutBtn_Click(object sender, RoutedEventArgs e)
        {
            About aboutWin = new About();
            aboutWin.Owner = this;
            aboutWin.ShowDialog();
        }

        private void StartStopBtn_Click(object sender, RoutedEventArgs e)
        {
            ChangeService();
        }

        private void openInit_Click(object sender, RoutedEventArgs e)
        {
            mainTabCon.SelectedIndex = 0;
        }

        public void myController_Click(object sender, RoutedEventArgs e)
        {
            conLvViewModel.setPrintingController(0);
            gamersProfile.UpdateLvViewModel(conLvViewModel);
            youListViewModel.selectYoutuber(0);
            gamersProfile.UpdateYoutubersViewModel(youListViewModel);
            gamersProfile.mainInfoPanel.Visibility = Visibility.Visible;
            gamersProfile.profilePanel.Visibility = Visibility.Collapsed;
            mainTabCon.SelectedIndex = 1;
            screenTag = 0;
            SelectionChanged();
        }

        public void openYoutubers_Click(object sender, RoutedEventArgs e)
        {
            mainTabCon.SelectedIndex = 2;
            screenTag = 1;
        }


        private void openYoutuber_Click (object sender, RoutedEventArgs e)
        {
            // Disable for the first release of RemottoApp on 29-05-2021
           /* Button youtuber = sender as Button;
            int index = (int)youtuber.Tag;
            youListViewModel.selectYoutuber(index);
            gamersProfile.UpdateYoutubersViewModel(youListViewModel);
            gamersProfile.BindIcons();

            mainTabCon.SelectedIndex = 1; */
        }

        private void openGames_CLick(object sender, RoutedEventArgs e)
        {
            mainTabCon.SelectedIndex = 3;
        }

        private void openHelpMe_CLick(object sender, RoutedEventArgs e)
        {
            mainTabCon.SelectedIndex = 4;
        }

        private async void ChangeService()
        {
            //StartStopBtn.IsEnabled = false;
            App root = Application.Current as App;
            //Tester service = root.rootHubtest;
            ControlService service = App.rootHub;
            await Task.Run(() =>
            {
                if (service.running)
                    service.Stop();
                else
                    service.Start();
            });
        }

        private void MainTabCon_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            gamersProfile.mainInfoPanel.Visibility = Visibility.Visible;
            gamersProfile.profilePanel.Visibility = Visibility.Hidden;
            if (mainTabCon.SelectedIndex == 0)
            {
                menuImage1.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/initActive.png", UriKind.Relative));
                textItem1.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans Heavy");
                menuImage2.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/init.png", UriKind.Relative));
                textItem2.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans");
                menuImage3.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/gamers.png", UriKind.Relative));
                textItem3.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans");
                menuImage4.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/games.png", UriKind.Relative));
                textItem4.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans");
                menuImage5.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/helpme.png", UriKind.Relative));
                textItem5.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans");
            }
            else if (mainTabCon.SelectedIndex == 1)
            {
                if (gamersProfile.YoutubersViewModel.PrintYoutuber[0] == gamersProfile.YoutubersViewModel.GamersCol[0])
                {
                    menuImage1.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/init.png", UriKind.Relative));
                    textItem1.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans");
                    menuImage2.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/initActive.png", UriKind.Relative));
                    textItem2.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans Heavy");
                    menuImage3.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/gamers.png", UriKind.Relative));
                    textItem3.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans");
                    menuImage4.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/games.png", UriKind.Relative));
                    textItem4.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans");
                    menuImage5.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/helpme.png", UriKind.Relative));
                    textItem5.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans");
                } 
                else 
                {
                    menuImage1.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/init.png", UriKind.Relative));
                    textItem1.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans");
                    menuImage2.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/init.png", UriKind.Relative));
                    textItem2.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans");
                    menuImage3.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/gamersActive.png", UriKind.Relative));
                    textItem3.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans Heavy");
                    menuImage4.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/games.png", UriKind.Relative));
                    textItem4.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans");
                    menuImage5.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/helpme.png", UriKind.Relative));
                    textItem5.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans");
                }
                
            }
            else if (mainTabCon.SelectedIndex == 2)
            {
                menuImage1.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/init.png", UriKind.Relative));
                textItem1.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans");
                menuImage2.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/init.png", UriKind.Relative));
                textItem2.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans");
                menuImage3.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/gamersActive.png", UriKind.Relative));
                textItem3.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans Heavy");
                menuImage4.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/games.png", UriKind.Relative));
                textItem4.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans");
                menuImage5.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/helpme.png", UriKind.Relative));
                textItem5.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans");
            }
            else if (mainTabCon.SelectedIndex == 3)
            {
                menuImage1.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/init.png", UriKind.Relative));
                textItem1.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans");
                menuImage2.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/init.png", UriKind.Relative));
                textItem2.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans"); ;
                menuImage3.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/gamers.png", UriKind.Relative));
                textItem3.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans");
                menuImage4.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/gamesActive.png", UriKind.Relative));
                textItem4.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans Heavy");
                menuImage5.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/helpme.png", UriKind.Relative));
                textItem5.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans");
            }
            else if (mainTabCon.SelectedIndex == 4)
            {
                menuImage1.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/init.png", UriKind.Relative));
                textItem1.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans");
                menuImage2.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/init.png", UriKind.Relative));
                textItem2.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans"); ;
                menuImage3.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/gamers.png", UriKind.Relative));
                textItem3.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans");
                menuImage4.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/games.png", UriKind.Relative));
                textItem4.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans");
                menuImage5.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/helpmeActive.png", UriKind.Relative));
                textItem5.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans Heavy");
            }
        }

        private void SelectionChanged() 
        {
            if (gamersProfile.YoutubersViewModel.PrintYoutuber[0] == gamersProfile.YoutubersViewModel.GamersCol[0])
            {
                menuImage1.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/init.png", UriKind.Relative));
                textItem1.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans");
                menuImage2.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/initActive.png", UriKind.Relative));
                textItem2.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans Heavy");
                menuImage3.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/gamers.png", UriKind.Relative));
                textItem3.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans");
                menuImage4.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/games.png", UriKind.Relative));
                textItem4.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans");
                menuImage5.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/helpme.png", UriKind.Relative));
                textItem5.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans");
            }
            else
            {
                menuImage1.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/initActive.png", UriKind.Relative));
                textItem1.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans");
                menuImage2.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/init.png", UriKind.Relative));
                textItem2.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans");
                menuImage3.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/gamersActive.png", UriKind.Relative));
                textItem3.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans Heavy");
                menuImage4.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/games.png", UriKind.Relative));
                textItem4.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans");
                menuImage5.Source = new BitmapImage(new Uri(@"/RemottoApp;component/Resources/navbar icons/helpme.png", UriKind.Relative));
                textItem5.FontFamily = new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Object Sans");
            }
        }

        private void ExportLogBtn_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.AddExtension = true;
            dialog.DefaultExt = ".txt";
            dialog.Filter = "Text Documents (*.txt)|*.txt";
            dialog.Title = "Select Export File";
            // TODO: Expose config dir
            dialog.InitialDirectory = Global.appdatapath;
            if (dialog.ShowDialog() == true)
            {
                LogWriter logWriter = new LogWriter(dialog.FileName, logvm.LogItems.ToList());
                logWriter.Process();
            }
        }

        private void IdColumnTxtB_ToolTipOpening(object sender, ToolTipEventArgs e)
        {
            TextBlock statusBk = sender as TextBlock;
            int idx = Convert.ToInt32(statusBk.Tag);
            if (idx >= 0)
            {
                CompositeDeviceModel item = conLvViewModel.ControllerDict[idx];
                item.RequestUpdatedTooltipID();
            }
        }   

        /// <summary>
        /// Clear and re-populate tray context menu
        /// </summary>
        private void NotifyIcon_TrayRightMouseUp(object sender, RoutedEventArgs e)
        {
            notifyIcon.ContextMenu = trayIconVM.ContextMenu;
        }

        private void CustomColorPick_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e) {}

        private void SelGamepadBtn_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            int idx = Convert.ToInt32(button.Tag);
            if (idx > -1 && conLvViewModel.ControllerDict.ContainsKey(idx))
            {
                conLvViewModel.setPrintingController(idx);
                mainTabCon.SelectedIndex = 1;
            }
        }

        private void MainDS4Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (editor != null)
            {
                editor.Close();
                e.Cancel = true;
                return;
            }
            else if (contextclose)
            {
                return;
            }
            else if (Global.CloseMini)
            {
                maxormin = true;
                WindowState = WindowState.Minimized;
                e.Cancel = true;
                return;
            }           

            e.Cancel = true;
            maxormin = false;
            Hide();
        }

        private void MainDS4Window_Closed(object sender, EventArgs e)
        {
                hotkeysTimer.Stop();
                autoProfilesTimer.Stop();
                autoProfileHolder.Save("Default");
                Util.UnregisterNotify(regHandle);
                Application.Current.Shutdown();  
        }

        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);

            HwndSource source = PresentationSource.FromVisual(this) as HwndSource;
            HookWindowMessages(source);
            source.AddHook(WndProc);
        }

        private bool inHotPlug = false;
        private int hotplugCounter = 0;
        private object hotplugCounterLock = new object();
        private const int DBT_DEVNODES_CHANGED = 0x0007;
        private const int DBT_DEVICEARRIVAL = 0x8000;
        private const int DBT_DEVICEREMOVECOMPLETE = 0x8004;
        public const int WM_COPYDATA = 0x004A;

        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam,
            IntPtr lParam, ref bool handled)
        {
            // Handle messages...
            switch (msg)
            {
                case Util.WM_DEVICECHANGE:
                {
                    if (Global.runHotPlug)
                    {
                        Int32 Type = wParam.ToInt32();
                        if (Type == DBT_DEVICEARRIVAL ||
                            Type == DBT_DEVICEREMOVECOMPLETE)
                        {
                            lock (hotplugCounterLock)
                            {
                                hotplugCounter++;
                            }

                            if (!inHotPlug)
                            {
                                inHotPlug = true;
                                Task.Run(() => { 
                                    InnerHotplug2(); 
                                });
                            }
                        }
                    }
                    break;
                }
                case WM_COPYDATA:
                {
                    // Received InterProcessCommunication (IPC) message. DS4Win command is embedded as a string value in lpData buffer
                    App.COPYDATASTRUCT cds = (App.COPYDATASTRUCT)Marshal.PtrToStructure(lParam, typeof(App.COPYDATASTRUCT));
                    if (cds.cbData >= 4 && cds.cbData <= 256)
                    {
                        int tdevice = -1;

                        byte[] buffer = new byte[cds.cbData];
                        Marshal.Copy(cds.lpData, buffer, 0, cds.cbData);
                        string[] strData = Encoding.ASCII.GetString(buffer).Split('.');

                        if (strData.Length >= 1)
                        {
                            strData[0] = strData[0].ToLower();

                            if (strData[0] == "start")
                                ChangeService();
                            else if (strData[0] == "stop")
                                ChangeService();
                            else if (strData[0] == "shutdown")
                                MainDS4Window_Closing(this, new System.ComponentModel.CancelEventArgs());
                            else if ((strData[0] == "loadprofile" || strData[0] == "loadtempprofile") && strData.Length >= 3)
                            {
                                // Command syntax: LoadProfile.device#.profileName (fex LoadProfile.1.GameSnake or LoadTempProfile.1.WebBrowserSet)
                                if (int.TryParse(strData[1], out tdevice)) tdevice--;

                                if (tdevice >= 0 && tdevice < ControlService.DS4_CONTROLLER_COUNT &&
                                        File.Exists(Global.appdatapath + "\\Profiles\\" + strData[2] + ".xml"))
                                {
                                    if (strData[0] == "loadprofile")
                                    {
                                        int idx = profileListHolder.ProfileListCol.Select((item, index) => new { item, index }).
                                                Where(x => x.item.Name == strData[2]).Select(x => x.index).DefaultIfEmpty(-1).First();

                                        if (idx >= 0 && tdevice < conLvViewModel.ControllerCol.Count)
                                        {
                                            conLvViewModel.ControllerCol[tdevice].ChangeSelectedProfile(strData[2]);
                                        }
                                        else
                                        {
                                            // Preset profile name for later loading
                                            Global.ProfilePath[tdevice] = strData[2];
                                            //Global.LoadProfile(tdevice, true, Program.rootHub);
                                        }
                                    }
                                    else
                                    {
                                        Global.LoadTempProfile(tdevice, strData[2], true, Program.rootHub);
                                    }

                                    Program.rootHub.LogDebug(Properties.Resources.UsingProfile.
                                        Replace("*number*", (tdevice + 1).ToString()).Replace("*Profile name*", strData[2]));
                                }
                            }
                        }
                    }
                    break;
                }
                default: break;
            }

            return IntPtr.Zero;
        }

        private void InnerHotplug2()
        {
            inHotPlug = true;

            bool loopHotplug = false;
            lock (hotplugCounterLock)
            {
                loopHotplug = hotplugCounter > 0;
            }

            while (loopHotplug == true)
            {
                Thread.Sleep(1500);
                Program.rootHub.HotPlug();
                //TaskRunner.Run(() => { Program.rootHub.HotPlug(uiContext); });
                lock (hotplugCounterLock)
                {
                    hotplugCounter--;
                    loopHotplug = hotplugCounter > 0;
                }
            }

            inHotPlug = false;
        }

        private void HookWindowMessages(HwndSource source)
        {
            Guid hidGuid = new Guid();
            NativeMethods.HidD_GetHidGuid(ref hidGuid);
            bool result = Util.RegisterNotify(source.Handle, hidGuid, ref regHandle);
            if (!result)
            {
                App.Current.Shutdown();
            }
        }

        private void addGame_Click(object sender, RoutedEventArgs e)
        {
            autoProfControl.BrowseProgsMenuItem_Click(sender, e);
        }

        private void ProfFolderBtn_Click(object sender, RoutedEventArgs e)
        {
            Process.Start(Global.appdatapath + "\\Profiles");
        }
        
        private void ControlPanelBtn_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("control", "joy.cpl");
        }

        private async void DriverSetupBtn_Click(object sender, RoutedEventArgs e)
        {
            //StartStopBtn.IsEnabled = false;
            await Task.Run(() =>
            {
                if (App.rootHub.running)
                    App.rootHub.Stop();
            });

            //StartStopBtn.IsEnabled = true;
            Process p = new Process();
            p.StartInfo.FileName = Global.exelocation;
            p.StartInfo.Arguments = "-driverinstall";
            p.StartInfo.Verb = "runas";
            try { p.Start(); }
            catch { }
        }

        private void CheckUpdatesBtn_Click(object sender, RoutedEventArgs e)
        {
            Task.Run(() =>
            {
                DownloadUpstreamVersionInfo();
                Check_Version(true);
            });
        }

        // settings function, erased.
        /*private void UseWhiteDS4IconCk_Click(object sender, RoutedEventArgs e)
        {
            bool status = useWhiteDS4IconCk.IsChecked == true;
            trayIconVM.IconSource = status ? TrayIconViewModel.ICON_WHITE : TrayIconViewModel.ICON_COLOR;
        }*/

        private void CheckDrivers()
        {
            bool driverinstalled = Global.IsViGEmBusInstalled();
            if (!driverinstalled)
            {
                Process p = new Process();
                p.StartInfo.FileName = $"{Global.exelocation}";
                p.StartInfo.Arguments = "-driverinstall";
                p.StartInfo.Verb = "runas";
                try { p.Start(); }
                catch { }
            }
        }

        private void MainDS4Window_StateChanged(object _sender, EventArgs _e)
        {
            CheckMinStatus();
        }

        public void CheckMinStatus()
        {
            bool minToTask = Global.MinToTaskbar;
            if (WindowState == WindowState.Minimized && !minToTask)
            {
                Hide();
                showAppInTaskbar = false;
            }
            else if (WindowState == WindowState.Normal && !minToTask)
            {
                Show();
                showAppInTaskbar = true;
            }
        }

        private void MainDS4Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (WindowState != WindowState.Minimized && preserveSize)
            {
                maxormin = true;
                Global.FormWidth = Convert.ToInt32(Width);
                Global.FormHeight = Convert.ToInt32(Height);
            }
        }

        private void MainDS4Window_LocationChanged(object sender, EventArgs e)
        {
            if (WindowState != WindowState.Minimized)
            {
                maxormin = true;
                Global.FormLocationX = Convert.ToInt32(Left);
                Global.FormLocationY = Convert.ToInt32(Top);
            }
        }

        private void NotifyIcon_TrayMiddleMouseDown(object sender, RoutedEventArgs e)
        {
            contextclose = true;
            Close();
        }
   
        private void NotifyIcon_TrayMouseDoubleClick(object sender, RoutedEventArgs e)
        {
            if (!showAppInTaskbar)
            {
                //Show();
                this.mainDS4Window.Show();
            }

            maxormin = true;
            WindowState = WindowState.Maximized;
        }

        private void Html5GameBtn_Click(object sender, RoutedEventArgs e)
        {
            Util.StartProcessHelper("https://html5gamepad.com/");
        }

        private void expandQuestion_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            TextBlock question = this.FindName("quest" + Convert.ToString(btn.Tag)) as TextBlock;
            TextBlock answer = this.FindName("question" + Convert.ToString(btn.Tag)) as TextBlock;
            TextBlock questionSym = this.FindName("question" + Convert.ToString(btn.Tag) + "Sym") as TextBlock;

            if (answer.Visibility == Visibility.Visible)
            {
                question.FontFamily = FindResource("ObjectSans") as FontFamily;
                answer.Visibility = Visibility.Collapsed;
                string tag = Convert.ToString(btn.Tag);
                switch (tag)
                {
                    case "1":
                        question1a.Visibility = Visibility.Collapsed;
                        break;
                    case "6":
                        question6a.Visibility = Visibility.Collapsed;
                        break;

                }
                questionSym.Text = "+";
            }
            else
            {
                question.FontFamily = FindResource("ObjectSansHeavy") as FontFamily;
                answer.Visibility = Visibility.Visible;
                string tag = Convert.ToString(btn.Tag);
                switch (tag)
                {                  
                    case "1":
                        question1a.Visibility = Visibility.Visible;
                        break;
                    case "6":
                        question6a.Visibility = Visibility.Visible;
                        break;

                }
                questionSym.Text = "-";
            }
        }
        private void RemottoBattery_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(e.Uri.AbsoluteUri);
        }

        private void SendMailTo(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }
    }
}
