﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Ookii.Dialogs.Wpf;
using RemottoApp.DS4Forms.ViewModels;
using Microsoft.Win32;

namespace RemottoApp.DS4Forms
{
    /// <summary>
    /// Interaction logic for AutoProfiles.xaml
    /// </summary>
    public partial class AutoProfiles : UserControl
    {
        protected String m_Profile = RemottoApp.Global.appdatapath + "\\Auto Profiles.xml";

        public const string steamCommx86Loc = @"C:\Program Files (x86)\Steam\steamapps\common";
        public const string steamCommLoc = @"C:\Program Files\Steam\steamapps\common";
        private string steamgamesdir;

        public const string ubisoftx86Loc = @"C:\Program Files (x86)\Ubisoft\Ubisoft Game Launcher\games";
        public const string ubisoftLoc = @"C:\Program Files\Ubisoft\Ubisoft Game Launcher\games";
        private string ubisoftgamesdir;

        public const string originx86Loc = @"C:\Program Files (x86)\Origin Games";
        public const string originLoc = @"C:\Program Files\Origin Games";
        private string origingamesdir;

        private string epicgamesdir = @"C:\Program Files (x86)\Epic Games\Launcher\Engine\Binaries\Win64"; 

        private AutoProfilesViewModel autoProfVM;
        private AutoProfileHolder autoProfileHolder;
        private ProfileList profileList;
        private bool autoDebug;

        public AutoProfileHolder AutoProfileHolder { get => autoProfileHolder;
            set => autoProfileHolder = value; }
        public AutoProfilesViewModel AutoProfVM { get => autoProfVM; }
        public bool AutoDebug { get => autoDebug; }
        public event EventHandler AutoDebugChanged;

        public AutoProfiles()
        {
            InitializeComponent();


            if (!File.Exists(RemottoApp.Global.appdatapath + @"\Auto Profiles.xml"))
                RemottoApp.Global.CreateAutoProfiles(m_Profile);

            steamDirectoyExists();
            ubisoftDirectoyExists();
            origintDirectoyExists();

            autoProfileHolder = new AutoProfileHolder();
        }

        private void steamDirectoyExists()
        {
            if (Directory.Exists(steamCommx86Loc))
                steamgamesdir = steamCommx86Loc;
            else if (Directory.Exists(steamCommLoc))
                steamgamesdir = steamCommLoc;
            else
                steamgamesdir = null;
        }

        private void ubisoftDirectoyExists()
        {
            if (Directory.Exists(ubisoftx86Loc))
                ubisoftgamesdir = ubisoftx86Loc;
            else if (Directory.Exists(ubisoftLoc))
                ubisoftgamesdir = ubisoftLoc;
            else
                ubisoftgamesdir = null;
        }

        private void origintDirectoyExists()
        {
            if (Directory.Exists(originx86Loc))
                origingamesdir = originx86Loc;
            else if (Directory.Exists(originLoc))
                origingamesdir = originLoc;
            else
                origingamesdir = null;
        }

        public void SetupDataContext(ProfileList profileList)
        {
            autoProfVM = new AutoProfilesViewModel(autoProfileHolder, profileList);
            programListLV.DataContext = autoProfVM;
            programListLV.ItemsSource = autoProfVM.ProgramColl;
            
            //revertDefaultProfileOnUnknownCk.DataContext = autoProfVM;

            autoProfVM.SearchFinished += AutoProfVM_SearchFinished;
            autoProfVM.CurrentItemChange += AutoProfVM_CurrentItemChange;

            this.profileList = profileList;
            /*cont1AutoProfCol.Collection = profileList.ProfileListCol;
            cont2AutoProfCol.Collection = profileList.ProfileListCol;
            cont3AutoProfCol.Collection = profileList.ProfileListCol;
            cont4AutoProfCol.Collection = profileList.ProfileListCol;*/
        }

        private void AutoProfVM_CurrentItemChange(AutoProfilesViewModel sender, ProgramItem item)
        {
            if (item != null)
            {
                if (item.MatchedAutoProfile != null)
                {
                    ProfileEntity tempProf = null;
                    string profileName = item.MatchedAutoProfile.ProfileNames[0];
                    if (!string.IsNullOrEmpty(profileName) && profileName != "(none)")
                    {
                        tempProf = profileList.ProfileListCol.SingleOrDefault(x => x.Name == profileName);
                        if (tempProf != null)
                        {
                            item.SelectedIndexCon1 = profileList.ProfileListCol.IndexOf(tempProf) + 1;
                        }
                    }
                    else
                    {
                        item.SelectedIndexCon1 = 0;
                    }

                    profileName = item.MatchedAutoProfile.ProfileNames[1];
                    if (!string.IsNullOrEmpty(profileName) && profileName != "(none)")
                    {
                        tempProf = profileList.ProfileListCol.SingleOrDefault(x => x.Name == profileName);
                        if (tempProf != null)
                        {
                            item.SelectedIndexCon2 = profileList.ProfileListCol.IndexOf(tempProf) + 1;
                        }
                    }
                    else
                    {
                        item.SelectedIndexCon2 = 0;
                    }

                    profileName = item.MatchedAutoProfile.ProfileNames[2];
                    if (!string.IsNullOrEmpty(profileName) && profileName != "(none)")
                    {
                        tempProf = profileList.ProfileListCol.SingleOrDefault(x => x.Name == profileName);
                        if (tempProf != null)
                        {
                            item.SelectedIndexCon3 = profileList.ProfileListCol.IndexOf(tempProf) + 1;
                        }
                    }
                    else
                    {
                        item.SelectedIndexCon3 = 0;
                    }

                    profileName = item.MatchedAutoProfile.ProfileNames[3];
                    if (!string.IsNullOrEmpty(profileName) && profileName != "(none)")
                    {
                        tempProf = profileList.ProfileListCol.SingleOrDefault(x => x.Name == profileName);
                        if (tempProf != null)
                        {
                            item.SelectedIndexCon4 = profileList.ProfileListCol.IndexOf(tempProf) + 1;
                        }
                    }
                    else
                    {
                        item.SelectedIndexCon4 = 0;
                    }
                }

                //editControlsPanel.DataContext = item;
                //editControlsPanel.IsEnabled = true;
            }
            
        }

        private void AutoProfVM_SearchFinished(object sender, EventArgs e)
        {
            IsEnabled = true;
            CreateAutoProfilesEntry();
            gamesText.Text = AutoProfVM.ProgramColl.Count.ToString();
        }

        public void LauncherMenuItem_Click(object sender, RoutedEventArgs e)
        {
            this.IsEnabled = true;
            //steamMenuItem.Visibility = Visibility.Collapsed;
            programListLV.ItemsSource = null;
            autoProfVM.SearchFinished += AppsSearchFinished;
            if (steamgamesdir != null)
                autoProfVM.AddProgramsFromLauncher(steamgamesdir);
            if (ubisoftgamesdir != null)
                autoProfVM.AddProgramsFromLauncher(ubisoftgamesdir);
            if (origingamesdir != null) 
                autoProfVM.AddProgramsFromLauncher(origingamesdir);
            if (epicgamesdir != null)
                autoProfVM.AddProgramsFromLauncher(epicgamesdir);
        }

        public void BrowseProgsMenuItem_Click(object sender, RoutedEventArgs e)
        {
            this.IsEnabled = false;
            VistaFolderBrowserDialog dialog = new VistaFolderBrowserDialog();
            if (dialog.ShowDialog() == true)
            {
                //browseProgsMenuItem.Visibility = Visibility.Collapsed;
                programListLV.ItemsSource = null;
                autoProfVM.SearchFinished += AppsSearchFinished;
                autoProfVM.AddProgramsFromDir(dialog.SelectedPath);
            }
            else
            {
                this.IsEnabled = true;
            }
        }

        private void StartMenuItem_Click(object sender, RoutedEventArgs e)
        {
            this.IsEnabled = false;
            //startMenuItem.Visibility = Visibility.Collapsed;
            programListLV.ItemsSource = null;
            autoProfVM.SearchFinished += AppsSearchFinished;
            autoProfVM.AddProgramsFromStartMenu();
        }

        private void AppsSearchFinished(object sender, EventArgs e)
        {
            autoProfVM.SearchFinished -= AppsSearchFinished;
            programListLV.ItemsSource = autoProfVM.ProgramColl;
        }

        private void AddProgramsBtn_Click(object sender, RoutedEventArgs e)
        {
            addProgramsBtn.ContextMenu.IsOpen = true;
            e.Handled = true;
        }

        private void HideUncheckedBtn_Click(object sender, RoutedEventArgs e)
        {
            programListLV.ItemsSource = null;
            autoProfVM.RemoveUnchecked();
            //steamMenuItem.Visibility = Visibility.Visible;
            startMenuItem.Visibility = Visibility.Visible;
            browseProgsMenuItem.Visibility = Visibility.Visible;
            programListLV.ItemsSource = autoProfVM.ProgramColl;
        }

        public void CreateAutoProfilesEntry()
        {
            foreach (ProgramItem progItem in autoProfVM.ProgramColl)
            {
                if (progItem.MatchedAutoProfile == null)
                {
                    autoProfVM.CreateAutoProfileEntry(progItem);
                }
                else
                {
                    autoProfVM.PersistAutoProfileEntry(progItem);
                }

                autoProfVM.AutoProfileHolder.Save(RemottoApp.Global.appdatapath + @"\Auto Profiles.xml");
            }
        }


        private void SaveAutoBtn_Click(object sender, RoutedEventArgs e)
        {
            if (autoProfVM.SelectedItem != null)
            {
                if (autoProfVM.SelectedItem.MatchedAutoProfile == null)
                {
                    autoProfVM.CreateAutoProfileEntry(autoProfVM.SelectedItem);
                }
                else
                {
                    autoProfVM.PersistAutoProfileEntry(autoProfVM.SelectedItem);
                }

                autoProfVM.AutoProfileHolder.Save(RemottoApp.Global.appdatapath + @"\Auto Profiles.xml");
            }
        }

        private void BrowseAddProgMenuItem_Click(object sender, RoutedEventArgs e)
        {
            this.IsEnabled = false;
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = false;
            dialog.AddExtension = true;
            dialog.DefaultExt = ".exe";
            dialog.Filter = "Program (*.exe)|*.exe";
            dialog.Title = "Select Program";

            dialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles);
            if (dialog.ShowDialog() == true)
            {
                programListLV.ItemsSource = null;
                autoProfVM.SearchFinished += AppsSearchFinished;
                autoProfVM.AddProgramExeLocation(dialog.FileName);
            }
            else
            {
                this.IsEnabled = true;
            }
        }

        private void MoveUpDownAutoBtn_Click(object sender, RoutedEventArgs e)
        {
            if (autoProfVM.SelectedItem != null && sender != null)
            {
                if(autoProfVM.MoveItemUpDown(autoProfVM.SelectedItem, ((sender as MenuItem).Name == "MoveUp") ? -1 : 1))
                    autoProfVM.AutoProfileHolder.Save(RemottoApp.Global.appdatapath + @"\Auto Profiles.xml");
            }
        }
    }
}
