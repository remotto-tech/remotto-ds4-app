﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Media;
using System.Windows.Interop;
using System.Windows;
using System.Windows.Media.Imaging;

namespace RemottoApp.DS4Forms.ViewModels
{
    public class SettingsViewModel
    {
        public bool HideDS4Controller { get => RemottoApp.Global.UseExclusiveMode;
            set => RemottoApp.Global.UseExclusiveMode = value; }

        public bool SwipeTouchSwitchProfile { get => RemottoApp.Global.SwipeProfiles;
            set => RemottoApp.Global.SwipeProfiles = value; }

        private bool runAtStartup;
        public bool RunAtStartup
        {
            get => runAtStartup;
            set
            {
                runAtStartup = value;
                RunAtStartupChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        public event EventHandler RunAtStartupChanged;

        private bool runStartProg;
        public bool RunStartProg
        {
            get => runStartProg;
            set
            {
                runStartProg = value;
                RunStartProgChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        public event EventHandler RunStartProgChanged;

        private bool runStartTask;
        public bool RunStartTask
        {
            get => runStartTask;
            set
            {
                runStartTask = value;
                RunStartTaskChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        public event EventHandler RunStartTaskChanged;

        private bool canWriteTask;
        public bool CanWriteTask { get => canWriteTask; }

        public ImageSource uacSource;
        public ImageSource UACSource { get => uacSource; }

        private Visibility showRunStartPanel = Visibility.Collapsed;
        public Visibility ShowRunStartPanel {
            get => showRunStartPanel;
            set
            {
                if (showRunStartPanel == value) return;
                showRunStartPanel = value;
                ShowRunStartPanelChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public event EventHandler ShowRunStartPanelChanged;

        public int ShowNotificationsIndex { get => RemottoApp.Global.Notifications; set => RemottoApp.Global.Notifications = value; }
        public bool DisconnectBTStop { get => RemottoApp.Global.DCBTatStop; set => RemottoApp.Global.DCBTatStop = value; }
        public bool FlashHighLatency { get => RemottoApp.Global.FlashWhenLate; set => RemottoApp.Global.FlashWhenLate = value; }
        public int FlashHighLatencyAt { get => RemottoApp.Global.FlashWhenLateAt; set => RemottoApp.Global.FlashWhenLateAt = value; }
        public bool StartMinimize { get => RemottoApp.Global.StartMinimized; set => RemottoApp.Global.StartMinimized = value; }
        public bool MinimizeToTaskbar { get => RemottoApp.Global.MinToTaskbar; set => RemottoApp.Global.MinToTaskbar = value; }
        public bool CloseMinimizes { get => RemottoApp.Global.CloseMini; set => RemottoApp.Global.CloseMini = value; }
        public bool QuickCharge { get => RemottoApp.Global.QuickCharge; set => RemottoApp.Global.QuickCharge = value; }
        public bool WhiteDS4Icon
        {
            get => RemottoApp.Global.UseWhiteIcon;
            set
            {
                if (RemottoApp.Global.UseWhiteIcon == value) return;
                RemottoApp.Global.UseWhiteIcon = value;
                WhiteDS4IconChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        public event EventHandler WhiteDS4IconChanged;

        public bool CheckForUpdates { get => RemottoApp.Global.CheckWhen > 0;
            set
            {
                RemottoApp.Global.CheckWhen = value ? 24 : 0;
                CheckForNoUpdatesWhen();
            }
        }
        public event EventHandler CheckForUpdatesChanged;

        public int CheckEvery { get
            {
                int temp = RemottoApp.Global.CheckWhen;
                if (temp > 23)
                {
                    temp = temp / 24;
                }
                return temp;
            }
            set
            {
                int temp;
                if (checkEveryUnitIdx == 0 && value < 24)
                {
                    temp = RemottoApp.Global.CheckWhen;
                    if (temp != value)
                    {
                        RemottoApp.Global.CheckWhen = value;
                        CheckForNoUpdatesWhen();
                    }
                }
                else if (checkEveryUnitIdx == 1)
                {
                    temp = RemottoApp.Global.CheckWhen / 24;
                    if (temp != value)
                    {
                        RemottoApp.Global.CheckWhen = value * 24;
                        CheckForNoUpdatesWhen();
                    }
                }
            }
        }
        public event EventHandler CheckEveryChanged;

        private int checkEveryUnitIdx = 1;
        public int CheckEveryUnit
        {
            get
            {
                return checkEveryUnitIdx;
            }
            set
            {
                if (checkEveryUnitIdx == value) return;
                checkEveryUnitIdx = value;
                CheckEveryUnitChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        public event EventHandler CheckEveryUnitChanged;
        public bool UseUDPServer
        {
            get => RemottoApp.Global.isUsingUDPServer();
            set
            {
                if (RemottoApp.Global.isUsingUDPServer() == value) return;
                RemottoApp.Global.setUsingUDPServer(value);
                UseUDPServerChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        public event EventHandler UseUDPServerChanged;

        public string UdpIpAddress { get => RemottoApp.Global.getUDPServerListenAddress();
            set => RemottoApp.Global.setUDPServerListenAddress(value); }
        public int UdpPort { get => RemottoApp.Global.getUDPServerPortNum(); set => RemottoApp.Global.setUDPServerPort(value); }
        public bool UseCustomSteamFolder
        {
            get => RemottoApp.Global.UseCustomSteamFolder;
            set
            {
                RemottoApp.Global.UseCustomSteamFolder = value;
                UseCustomSteamFolderChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        public event EventHandler UseCustomSteamFolderChanged;

        public string CustomSteamFolder
        {
            get => RemottoApp.Global.CustomSteamFolder;
            set
            {
                string temp = RemottoApp.Global.CustomSteamFolder;
                if (temp == value) return;
                if (Directory.Exists(value) || value == string.Empty)
                {
                    RemottoApp.Global.CustomSteamFolder = value;
                }
            }
        }

        private bool viewEnabled = true;
        public bool ViewEnabled
        {
            get => viewEnabled;
            set
            {
                viewEnabled = value;
                ViewEnabledChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        public event EventHandler ViewEnabledChanged;


        public SettingsViewModel()
        {
            checkEveryUnitIdx = 1;

            int checklapse = RemottoApp.Global.CheckWhen;
            if (checklapse < 24 && checklapse > 0)
            {
                checkEveryUnitIdx = 0;
            }

            CheckStartupOptions();

            Icon img = SystemIcons.Shield;
            Bitmap bitmap = img.ToBitmap();
            IntPtr hBitmap = bitmap.GetHbitmap();

            ImageSource wpfBitmap =
                 Imaging.CreateBitmapSourceFromHBitmap(
                      hBitmap, IntPtr.Zero, Int32Rect.Empty,
                      BitmapSizeOptions.FromEmptyOptions());
            uacSource = wpfBitmap;

            runAtStartup = StartupMethods.RunAtStartup();
            runStartProg = StartupMethods.HasStartProgEntry();
            runStartTask = StartupMethods.HasTaskEntry();
            canWriteTask = RemottoApp.Global.IsAdministrator();

            if (!runAtStartup)
            {
                runStartProg = true;
            }
            else if (runStartProg && runStartTask)
            {
                runStartProg = false;
                if (StartupMethods.CanWriteStartEntry())
                {
                    StartupMethods.DeleteStartProgEntry();
                }
            }

            if (runAtStartup && runStartProg)
            {
                bool locChange = StartupMethods.CheckStartupExeLocation();
                if (locChange)
                {
                    if (StartupMethods.CanWriteStartEntry())
                    {
                        StartupMethods.DeleteStartProgEntry();
                        StartupMethods.WriteStartProgEntry();
                    }
                    else
                    {
                        runAtStartup = false;
                        showRunStartPanel = Visibility.Collapsed;
                    }
                }
            }
            else if (runAtStartup && runStartTask)
            {
                if (canWriteTask)
                {
                    StartupMethods.DeleteOldTaskEntry();
                    StartupMethods.WriteTaskEntry();
                }
            }

            if (runAtStartup)
            {
                showRunStartPanel = Visibility.Visible;
            }

            RunAtStartupChanged += SettingsViewModel_RunAtStartupChanged;
            RunStartProgChanged += SettingsViewModel_RunStartProgChanged;
            RunStartTaskChanged += SettingsViewModel_RunStartTaskChanged;

            //CheckForUpdatesChanged += SettingsViewModel_CheckForUpdatesChanged;
            InitializeSettings();
        }

        private void SettingsViewModel_RunStartTaskChanged(object sender, EventArgs e)
        {
            if (runStartTask)
            {
                StartupMethods.WriteTaskEntry();
            }
            else
            {
                StartupMethods.DeleteTaskEntry();
            }
        }

        private void InitializeSettings()
        {
            DisconnectBTStop = true;
            HideDS4Controller = true;
            FlashHighLatency = true;
            MinimizeToTaskbar = true;
            FlashHighLatencyAt = 10;
            CheckForUpdates = true;
            CheckEveryUnit = 1;
            CheckEvery = 7;
        }
        
        private void SettingsViewModel_RunStartProgChanged(object sender, EventArgs e)
        {
            if (runStartProg)
            {
                StartupMethods.WriteStartProgEntry();
            }
            else
            {
                StartupMethods.DeleteStartProgEntry();
            }
        }

        private void SettingsViewModel_RunAtStartupChanged(object sender, EventArgs e)
        {
            if (runAtStartup)
            {
                RunStartProg = true;
                RunStartTask = false;
            }
            else
            {
                StartupMethods.DeleteStartProgEntry();
                StartupMethods.DeleteTaskEntry();
            }
        }

        private void SettingsViewModel_CheckForUpdatesChanged(object sender, EventArgs e)
        {
            if (!CheckForUpdates)
            {
                CheckEveryChanged?.Invoke(this, EventArgs.Empty);
                CheckEveryUnitChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        private void CheckStartupOptions()
        {
            bool lnkExists = File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Startup) + "\\DS4Windows.lnk");
            if (lnkExists)
            {
                runAtStartup = true;
            }
            else
            {
                runAtStartup = false;
            }
        }

        private void CheckForNoUpdatesWhen()
        {
            if (RemottoApp.Global.CheckWhen == 0)
            {
                checkEveryUnitIdx = 1;
            }

            CheckForUpdatesChanged?.Invoke(this, EventArgs.Empty);
            CheckEveryChanged?.Invoke(this, EventArgs.Empty);
            CheckEveryUnitChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}
