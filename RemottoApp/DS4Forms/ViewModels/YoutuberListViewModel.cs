﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Drawing;
using System.Runtime.CompilerServices;
using Windows.UI.WebUI;

namespace RemottoApp.DS4Forms.ViewModels
{
    public class YoutuberListViewModel
    {
        private ObservableCollection<YoutuberViewModel> gamersCol =
           new ObservableCollection<YoutuberViewModel>();
        private ObservableCollection<YoutuberViewModel> youtubersCol =
           new ObservableCollection<YoutuberViewModel>();
        private ObservableCollection<YoutuberViewModel> printYoutuber =
           new ObservableCollection<YoutuberViewModel>();
        private ObservableCollection<YoutuberViewModel> youtubersColWithoutSelected =
   new ObservableCollection<YoutuberViewModel>();
        private YoutuberViewModel selectedYoutuber;
        // private int selectedIndex = -1;

        public ObservableCollection<YoutuberViewModel> GamersCol { get => gamersCol; }
        public ObservableCollection<YoutuberViewModel> YoutubersCol { get => youtubersCol; }
        public ObservableCollection<YoutuberViewModel> PrintYoutuber { get => printYoutuber; }
        // public int SelectedIndex { get => selectedIndex; set => selectedIndex = value; }
        public ObservableCollection<YoutuberViewModel> YoutubersColWithoutSelected { get => youtubersColWithoutSelected; }

        public void populateYoutubersColWithoutSelected() {
            youtubersColWithoutSelected = new ObservableCollection<YoutuberViewModel>();
            foreach (YoutuberViewModel item in youtubersCol)
            {
                if (item != printYoutuber[0])
                {
                    youtubersColWithoutSelected.Add(item);
                }
            }
        }

        public YoutuberListViewModel()
        {
            gamersCol = new ObservableCollection<YoutuberViewModel>();
            Dictionary<String, Tuple<List<String>, String, bool>> youtubeDict = new Dictionary<String, Tuple<List<String>, String, Boolean>>();

            List<String> user_games = new List<string>();
            Tuple<List<String>, String, bool> default_tuple = new Tuple<List<String>, String, bool>(user_games,"",true);
            youtubeDict.Add("Default", default_tuple);

            List<String> auron_games = new List<String>();
            auron_games.Add("Gta5");
            List<String> ampeter_games = new List<String>();
            ampeter_games.Add("Fortnite");
            ampeter_games.Add("Valorant");

            Tuple<List<String>, String, bool> auron_tuple = new Tuple<List<String>, String, bool> (auron_games, "Mando XController Edicion XX", false);
            Tuple<List<String>, String, bool> ampeter_tuple = new Tuple<List<String>, String, bool> (ampeter_games, "Mando XController Edicion X", false);
            youtubeDict.Add("Ampeterby7",ampeter_tuple);
            youtubeDict.Add("TheGrefg", ampeter_tuple);
            youtubeDict.Add("Agustin51", ampeter_tuple);
            youtubeDict.Add("StaXx", auron_tuple);
            youtubeDict.Add("Auronplay", auron_tuple);

            populateCurrentEntries(youtubeDict);
        }

        private void populateCurrentEntries(Dictionary<String, Tuple<List<String>, String, bool>> youtubeDict)
        {
            int index = 0;
            foreach(var item in youtubeDict)
            {
                YoutuberViewModel newYoutuber = new YoutuberViewModel(item.Key,item.Value.Item1,item.Value.Item2,item.Value.Item3,index);
                gamersCol.Add(newYoutuber);
                
                if (item.Key != "Default")
                    youtubersCol.Add(newYoutuber);

                index++;
            }
            printYoutuber.Add(GamersCol[0]);
            
        }

        public void RemoveYoutubeEntry(YoutuberViewModel youtuber)
        {
            gamersCol.Remove(youtuber);
        }

        public void selectYoutuber(int index) 
        {
            if (gamersCol.Count != 0)
            {
                if (printYoutuber.Count != 0)
                {
                    printYoutuber[0] = gamersCol[index];
                }
                else
                {
                    printYoutuber.Add(GamersCol[index]);
                }
            }
        }

        public void changePlayerName(string name)
        {
            PrintYoutuber[0].setName(name);
        }
    }
    public class YoutuberViewModel
    {
        private String youtuberIcon;
        private String name;
        private String controllerName;
        private String controllerIcon;
        private List<String> games;
        private List<String> gamesIcon;
        private List<String> profiles;
        private String gamesString;
        private int index;
        private Boolean selfUser;

        public String YoutuberIcon { get => youtuberIcon; }
        public String Name { get => name; }
        public String ControllerName { get => controllerName; }
        public String ControllerIcon { get => controllerIcon; }
        public List<String> Profiles { get => profiles; }
        public List<String> Games { get => games; }
        public List<String> GamesIcon { get => gamesIcon; }
        public String GamesString { get => gamesString; }
        public int Index { get => index; }

        public YoutuberViewModel(String name, List<String> games, String controllerName, bool selfUser,int index)
        {
            String projectFolder = @"/RemottoApp;component/Resources/";
            this.controllerIcon = projectFolder + @"gamepads/" + name + ".png";
            this.controllerName = controllerName;
            this.games = games;

            if (name == "Default") {
                this.name = Properties.Settings.Default.UserName;
                if (Properties.Settings.Default.ProfilePic == "ProfilePic")
                {
                    Random r = new Random();
                    int rInt = r.Next(5, 10);
                    Properties.Settings.Default.ProfilePic = projectFolder + @"youtuber icons/avatars-0" + rInt + ".png";
                    Properties.Settings.Default.Save();
                }

                this.youtuberIcon = Properties.Settings.Default.ProfilePic;
            }
            else {
                this.name = name;
                this.youtuberIcon = projectFolder + @"youtuber icons/" + name + ".png";
            }

            this.index = index;
            this.selfUser = selfUser;
            this.gamesIcon = new List<String>();
            this.profiles = new List<String>();
            
            foreach (String game in games)
            {
                this.profiles.Add(projectFolder + @"profiles/" + this.name + @"/" + game + ".png");
                this.gamesIcon.Add(projectFolder + @"game icons/" + game + ".png");
                if (game != "default") 
                { 
                    if (games.IndexOf(game) < games.Count() - 1) 
                    {
                        gamesString += game + ", ";
                    }
                    else
                    {
                        gamesString += game;
                    }
                } 
                else
                {
                    gamesString = "All games.";
                }
            }
        }

        public void setName(string name) 
        {
            this.name = name;
        }

        public Visibility IsYoutuber
        {
            get
            {
                if (selfUser)
                    return Visibility.Collapsed;
                else
                    return Visibility.Visible;
            }
        }

        public Visibility IsYoutuberHidden
        {
            get
            {
                if (selfUser)
                    return Visibility.Hidden;
                else
                    return Visibility.Visible;
            }
        }

        public Visibility IsSelfPlayer
        {
            get
            {
                if (selfUser)
                    return Visibility.Visible;
                else
                    return Visibility.Hidden;
            }
        }

        public Visibility IsSelfPlayerCollapsed
        {
            get
            {
                if (selfUser)
                    return Visibility.Visible;
                else
                    return Visibility.Collapsed;
            }
        }

    }
}
