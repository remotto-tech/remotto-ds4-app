﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RemottoApp.DS4Forms
{
    /// <summary>
    /// Interaction logic for lagTips.xaml
    /// </summary>
    public partial class lagTips : Window
    {
        private MainWindow mainWindow;
        public lagTips()
        {
            InitializeComponent();
            
        }

        public void setMainWindow(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
            mainWindow.Opacity = 0.4;

        }

        private void CloseTips(object sender, RoutedEventArgs e)
        {

            mainWindow.Opacity = 1;
            Close();

        }

        private void OpenRequestWeb(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://es.remottobattery.com/products/remotto-battery");
        }

        protected override void OnDeactivated(EventArgs e)
        {
            mainWindow.Opacity = 1;
            this.Hide();
            base.OnDeactivated(e);

        }

    }


    
}
