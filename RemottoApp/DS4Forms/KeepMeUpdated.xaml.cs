﻿using Garlic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RemottoApp.DS4Forms
{
    /// <summary>
    /// Interaction logic for KeepMeUpdated.xaml
    /// </summary>
    public partial class KeepMeUpdated : Window
    {
        private MainWindow mainWindow;
        private AnalyticsSession analyticsSession;

        public KeepMeUpdated()
        {
            InitializeComponent();
        }


        public void setMainWindow(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
            mainWindow.Opacity = 0.4;

        }

        private void CloseTips(object sender, RoutedEventArgs e)
        {

            mainWindow.Opacity = 1;
            Close();

        }

        public void SendMailAnalytics(object sender, RoutedEventArgs e)
        {

            if(SendMail.Text == "")
            {

            }
            else
            {
                try
                {
                    MailAddress m = new MailAddress(SendMail.Text);


                    analyticsSession = new AnalyticsSession("remottopcapp.com", "UA-73168939-4");
                    //analyticsSession = new AnalyticsSession("remottopcapp.com", "UA-73168939-4");

                    analyticsSession.SetCustomVariable(3, "keepmeupdated", "help@remottobattery.com");

                    var page = analyticsSession.CreatePageViewRequest("/", "Main");
                    page.SendEvent("keepmeupdated", "keepmeupdated", SendMail.Text, "1");

                    mainWindow.Opacity = 1;
                    Close();

                    //return true;
                }
                catch (FormatException)
                {
                    Validated.Visibility = Visibility.Visible;
                    //return false;
                }



            }







        }

        protected override void OnDeactivated(EventArgs e)
        {
            mainWindow.Opacity = 1;
            this.Hide();
            base.OnDeactivated(e);

        }




    }
}
